window.charterQuery = {
    abs: {
        AUTH: {
            login: "travelsoft",
            password: "hdyNb81Uf-q&51M"
        },
        GET_TOKEN: "http://online.abstour.by:88/ChartersWin64/api/Charter/GetToken",
        GET_FROM_CITY: "http://online.abstour.by:88/ChartersWin64/api/Charter/GetDepartureCities",
        GET_TO_CITY: "http://online.abstour.by:88/ChartersWin64/api/Charter/GetArrivalCities",
        GET_DATE_FROM: "http://online.abstour.by:88/ChartersWin64/api/Charter/GetStraightFlightDates",
        GET_CHARTER: "http://online.abstour.by:88/ChartersWin64/api/Charter/GetPeriodFlights",
        GET_CHARTER_ONE_WAY: "http://online.abstour.by:88/ChartersWin64/api/Charter/GetFlights"
    },
    alatan: {
        AUTH: {
            login: "travelsoft",
            password: "hdyNb81Uf-q&51M"
        },
        GET_TOKEN: "https://online.alatantour.by/charter/api/Charter/GetToken",
        GET_FROM_CITY: "https://online.alatantour.by/charter/api/Charter/GetDepartureCities",
        GET_TO_CITY: "https://online.alatantour.by/charter/api/Charter/GetArrivalCities",
        GET_DATE_FROM: "https://online.alatantour.by/charter/api/Charter/GetStraightFlightDates",
        GET_CHARTER: "https://online.alatantour.by/charter/api/Charter/GetPeriodFlights",
        GET_CHARTER_ONE_WAY: "https://online.alatantour.by/charter/api/Charter/GetFlights"
    }
};
window.tourConfig = '';
window.charterActionSend = '/ajax/order_charter.php';
window.charterUrl = "/aviabilety/charternye-bilety/";
window.stringfy = true;
window.AxiosPost = {
    'method':'POST',
    'cors':'no-cors',
    'Accept': 'application/x-www-form-urlencoded',
    'Content-Type': 'application/x-www-form-urlencoded'
};

window.FormMessage = {
    MESSAGE_TABS:{
        avia:{
            title:'Авиа билеты',
            key:'avia',
            active:true
        },
        charter:{
            title:'Чартерные билеты',
            key:'charter',
            active:true
        },
        tours:{
            title:'Авиа туры',
            key:'tours',
            active:true
        },
        busTours:{
            title:'Автобусные туры',
            key:'busTours',
            active:false
        },
    },

    MESSAGE_CHARTER: {
        auth: {
            tittle: null,
            placeholder: null,
            error: "Ошибка авторизации",
            warning: null,
            popup: null,
            loading: "Авторизация"
        },
        charter: {
            tittle: null,
            placeholder: null,
            error: "Ошибка поиска чартеров",
            warning: "Извините но по данному направления чартеты отсуствуют",
            popup: null,
            loading: "Поиск чартеров"
        },
        from: {
            tittle: "Откуда",
            placeholder: "Укажите пункт вылета",
            error: "Ошибка получения городов вылета",
            warning: "Извините но по данному направления чартеты отсуствуют",
            popup: "Укажите пункт вылета",
            loading: "Загрузка путктов вылета"
        },
        to: {
            tittle: "Куда",
            placeholder: "Укажите пункт прилета",
            error: "Ошибка получения городов прилета",
            warning: "Извините но по данному направления чартеты отсуствуют",
            popup: "Укажите пункт прилета",
            loading: "Загрузка путктов прилета"
        },
        date: {
            tittle: "Дата",
            placeholder: ["Периуд отправления","Отправление с"],
            error: "Ошибка получения городов прилета",
            warning: "Извините но по данному направления чартеты отсуствуют",
            popup: ["Период вылета", "Дата отправления"],
            loading: "Загрузка дат"
        },
        night: {
            tittle: "Ночей",
            placeholder: "Ночей с - по",
            error: null,
            warning: null,
            popup: "Укажите колечство ночей с - по",
            loading: null
        },
        people: {
            tittle: "Пассажиров",
            placeholder: "Укажите количество взрослых и детей",
            error: null,
            warning: null,
            popup: "Укажите количество взрослых и детей",
            loading: null
        },
        submit: {
            tittle: "Поиск",
            placeholder: null,
            error: null,
            warning: null,
            popup: null,
            loading: null
        },
        result: {
            host: "Оператор",
            class: "Класс",
            way_from: "Рейс туда",
            way_back: "Рейс обратно",
            time_from: "Время туда",
            time_back: "Время обратно"
        }
    },
    MESSAGE_AVIA: {
        from: {
            tittle: "Откуда",
            placeholder: "Укажите пункт вылета",
            error: null,
            warning: null,
            popup: null,
            loading: null
        },
        to: {
            tittle: "Куда",
            placeholder: "Укажите пункт прилета",
            error: null,
            warning: null,
            popup: null,
            loading: null
        },
        date_from: {
            tittle: "Дата туда",
            placeholder: "Дату туда",
            error: null,
            warning: null,
            popup: "Укажите дату туда",
            loading: null
        },
        date_to: {
            tittle: "Дата обратно",
            placeholder: "Даты обратно",
            error: null,
            warning: null,
            popup: "Укажите дату обратно",
            loading: null
        },
        people: {
            tittle: "Пассажиров",
            placeholder: "Укажите количество взрослых и детей",
            error: null,
            warning: null,
            popup: "Укажите количество взрослых и детей",
            loading: null
        },
        class: {
            tittle: "Класс",
            placeholder: "Укажите желаймый класс",
            error: null,
            warning: null,
            popup: "Укажите желаймый класс",
            loading: null
        },
        submit: {
            tittle: "Поиск",
            placeholder: null,
            error: null,
            warning: null,
            popup: null,
            loading: null
        }
    }
};
