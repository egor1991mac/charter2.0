
$(document).ready( function(e){
    /*let sc = document.querySelectorAll('script');
  [].slice.call(sc).forEach(item=>{
      let src = item.getAttribute('src');
      if(src != null){
          if(src.indexOf('tourvisor') != -1){
              item.remove();
          }
      }
  });*/
    function downloadJSAtOnload(scripts) {
        var element;
        for(var i = 0; i < scripts.length; i++){
            element = document.createElement("script");
            element.src = scripts[i];
            element.type = 'text/javascript';
            document.body.appendChild(element);
            console.log(element);
        }
    }
    function loadCss(filename) {
        filename.forEach(item=>{
            var l = document.createElement('link');
            l.rel = 'stylesheet';
            l.href = item;
            var h = document.getElementsByTagName('head')[0];
            h.parentNode.insertBefore(l, h);
        })

    }

    var Path = window.componentPath;


        ! function(i) {
            function e(e) {
                for (var t, r, n = e[0], o = e[1], a = e[2], c = 0, u = []; c < n.length; c++) r = n[c], d[r] && u.push(d[r][0]), d[r] = 0;
                for (t in o) Object.prototype.hasOwnProperty.call(o, t) && (i[t] = o[t]);
                for (h && h(e); u.length;) u.shift()();
                return l.push.apply(l, a || []), f()
            }

            function f() {
                for (var e, t = 0; t < l.length; t++) {
                    for (var r = l[t], n = !0, o = 1; o < r.length; o++) {
                        var a = r[o];
                        0 !== d[a] && (n = !1)
                    }
                    n && (l.splice(t--, 1), e = p(p.s = r[0]))
                }
                return e
            }
            var r = {},
                s = {
                    5: 0
                },
                d = {
                    5: 0
                },
                l = [];

            function p(e) {
                if (r[e]) return r[e].exports;
                var t = r[e] = {
                    i: e,
                    l: !1,
                    exports: {}
                };
                return i[e].call(t.exports, t, t.exports, p), t.l = !0, t.exports
            }
            p.e = function(l) {
                var e = [];
                s[l] ? e.push(s[l]) : 0 !== s[l] && {
                    2: 1,
                    8: 1,
                    10: 1,
                    11: 1,
                    12: 1
                }[l] && e.push(s[l] = new Promise(function(e, n) {
                    for (var t = "charterForm/static/css/" + ({}[l] || l) + "." + {
                        0: "31d6cfe0",
                        1: "31d6cfe0",
                        2: "f4c0141c",
                        3: "31d6cfe0",
                        7: "31d6cfe0",
                        8: "bbc2948a",
                        9: "31d6cfe0",
                        10: "396493db",
                        11: "c33eb381",
                        12: "83a105f8"
                    }[l] + ".chunk.css", o = p.p + t, r = document.getElementsByTagName("link"), a = 0; a < r.length; a++) {
                        var c = (i = r[a]).getAttribute("data-href") || i.getAttribute("href");
                        if ("stylesheet" === i.rel && (c === t || c === o)) return e()
                    }
                    var u = document.getElementsByTagName("style");
                    for (a = 0; a < u.length; a++) {
                        var i;
                        if ((c = (i = u[a]).getAttribute("data-href")) === t || c === o) return e()
                    }
                    var f = document.createElement("link");
                    f.rel = "stylesheet", f.type = "text/css", f.onload = e, f.onerror = function(e) {
                        var t = e && e.target && e.target.src || o,
                            r = new Error("Loading CSS chunk " + l + " failed.\n(" + t + ")");
                        r.request = t, delete s[l], f.parentNode.removeChild(f), n(r)
                    }, f.href = o, document.getElementsByTagName("head")[0].appendChild(f)
                }).then(function() {
                    s[l] = 0
                }));
                var r = d[l];
                if (0 !== r)
                    if (r) e.push(r[2]);
                    else {
                        var t = new Promise(function(e, t) {
                            r = d[l] = [e, t]
                        });
                        e.push(r[2] = t);
                        var n, a = document.createElement("script");
                        a.charset = "utf-8", a.timeout = 120, p.nc && a.setAttribute("nonce", p.nc), a.src = p.p + "charterForm/static/js/" + ({}[l] || l) + "." + {
                            0: "429e7152",
                            1: "6ebdfe47",
                            2: "abe58fc8",
                            3: "386ab565",
                            7: "4a45326a",
                            8: "ec0ccc29",
                            9: "2834203f",
                            10: "dc273c97",
                            11: "3dbd85cb",
                            12: "5abdd1f8"
                        }[l] + ".chunk.js", n = function(e) {
                            a.onerror = a.onload = null, clearTimeout(c);
                            var t = d[l];
                            if (0 !== t) {
                                if (t) {
                                    var r = e && ("load" === e.type ? "missing" : e.type),
                                        n = e && e.target && e.target.src,
                                        o = new Error("Loading chunk " + l + " failed.\n(" + r + ": " + n + ")");
                                    o.type = r, o.request = n, t[1](o)
                                }
                                d[l] = void 0
                            }
                        };
                        var c = setTimeout(function() {
                            n({
                                type: "timeout",
                                target: a
                            })
                        }, 12e4);
                        a.onerror = a.onload = n, document.head.appendChild(a)
                    }
                return Promise.all(e)
            }, p.m = i, p.c = r, p.d = function(e, t, r) {
                p.o(e, t) || Object.defineProperty(e, t, {
                    enumerable: !0,
                    get: r
                })
            }, p.r = function(e) {
                "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
                    value: "Module"
                }), Object.defineProperty(e, "__esModule", {
                    value: !0
                })
            }, p.t = function(t, e) {
                if (1 & e && (t = p(t)), 8 & e) return t;
                if (4 & e && "object" == typeof t && t && t.__esModule) return t;
                var r = Object.create(null);
                if (p.r(r), Object.defineProperty(r, "default", {
                    enumerable: !0,
                    value: t
                }), 2 & e && "string" != typeof t)
                    for (var n in t) p.d(r, n, function(e) {
                        return t[e]
                    }.bind(null, n));
                return r
            }, p.n = function(e) {
                var t = e && e.__esModule ? function() {
                    return e.default
                } : function() {
                    return e
                };
                return p.d(t, "a", t), t
            }, p.o = function(e, t) {
                return Object.prototype.hasOwnProperty.call(e, t)
            }, p.p = "/", p.oe = function(e) {
                throw console.error(e), e
            };
            var t = window.webpackJsonp = window.webpackJsonp || [],
                n = t.push.bind(t);
            t.push = e, t = t.slice();
            for (var o = 0; o < t.length; o++) e(t[o]);
            var h = n;
            f()
        }([]);

        var scripts = [
            `${window.componentPath}charterForm/config.js`,
            `${window.componentPath}charterForm/static/js/6.6e110333.chunk.js`,
            `${window.componentPath}charterForm/static/js/main.06c23c6c.chunk.js`
        ];
        var css = [
            `${window.componentPath}charterForm/static/css/6.cb730ec0.chunk.css`,
            `${window.componentPath}charterForm/static/css/main.25235771.chunk.css`
        ];
        loadCss(css);
        downloadJSAtOnload(scripts);





});

