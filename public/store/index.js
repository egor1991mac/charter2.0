const initialState = {
    nav: [{
        CHAIN: ["Туры"],
        DEPTH_LEVEL: 1,
        IS_PARENT: false,
        ITEM_TYPE: "D",
        LINK: "/tours/",
        SELECTED: false,
        TEXT: "Туры",
    },
        {
            CHAIN: ["Туры"],
            DEPTH_LEVEL: 1,
            IS_PARENT: false,
            ITEM_TYPE: "D",
            LINK: "/tours/",
            SELECTED: false,
            TEXT: "Афиша",
        },
        {
            CHAIN: ["Туры"],
            DEPTH_LEVEL: 1,
            IS_PARENT: false,
            ITEM_TYPE: "D",
            LINK: "/tours/",
            SELECTED: false,
            TEXT: "Блог",
        },{
            CHAIN: ["Туры"],
            DEPTH_LEVEL: 1,
            IS_PARENT: false,
            ITEM_TYPE: "D",
            LINK: "/tours/",
            SELECTED: false,
            TEXT: "Путеводитель",
        },
        {
            CHAIN: ["Авиабилеты"],
            DEPTH_LEVEL: 1,
            IS_PARENT: true,
            ITEM_INDEX: 1,
            LINK: "/aviabilety/",
            PARAMS: [],
            PERMISSION: "R",
            SELECTED: false,
            TEXT: "Авиабилеты"
        },
        {
            CHAIN: ["Авиабилеты", "Регулярные авиалинии"],
            DEPTH_LEVEL: 2,
            IS_PARENT: true,
            ITEM_INDEX: 1,
            LINK: "/aviabilety/",
            PARAMS: [],
            PERMISSION: "R",
            SELECTED: false,
            TEXT: "Регулярные авиалинии"
        },
        {
            CHAIN: ["Авиабилеты", "Чартерные авиалинии"],
            DEPTH_LEVEL: 2,
            IS_PARENT: true,
            ITEM_INDEX: 1,
            LINK: "/aviabilety/",
            PARAMS: [],
            PERMISSION: "R",
            SELECTED: false,
            TEXT: "Чартерные авиалинии"
        }
    ],
    logo:'TIO.BY'
};
function Navigation(state = {data: Api.groupeMenu(initialState.nav)}, action) {

    if (action.type === 'SELECT_NAV' && action.payload){
        window.location.href = action.payload;
    }
    return state;
};

function Logo (state = {data: []}) {
    return state;
};

function OpenMobileMenu (state = false, action){
    if (action.type === 'OPEN_MOBILE_MENU') {
        state = !state;
    }
    return state;

};
function From(state=[],{type,payload}) {
    switch (type) {
        case 'GET_FROM':
            if(!isEqual(state[0],payload.data)){
                state = [payload.data];
            }

            break;
    }
    return state;
};
function Selected_From(state = [],{type,payload}) {
    switch (type) {
        case 'SELECTED_FROM':
            state = payload;
            break;
    }
    return state;
};
window.Reducers = {Navigation,Logo, OpenMobileMenu, From, Selected_From};

