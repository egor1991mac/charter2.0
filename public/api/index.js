
var Api =  {
    initApp: (globalObject, callback) =>{

            const {createStore, combineReducers} = window.Redux;
            let stor = function(){};
            if(globalObject.name[globalObject.propertyName] != undefined
                && Object.keys(globalObject.name[globalObject.propertyName]).length > 0){
                stor = createStore(combineReducers(globalObject.name[globalObject.propertyName]),
                    window.__REDUX_DEVTOOLS_EXTENSION__
                    && window.__REDUX_DEVTOOLS_EXTENSION__());
            }
            else{
                Object.defineProperty(globalObject.name, globalObject.propertyName, {
                    set: (val) => {
                        if (val) {
                            stor = createStore(combineReducers(val),
                                window.__REDUX_DEVTOOLS_EXTENSION__
                                && window.__REDUX_DEVTOOLS_EXTENSION__());
                        }
                    }
                })

            }
            Object.defineProperty(globalObject.name, globalObject.propertyName, {
                set: (val) => {
                    if (val) {
                        stor = createStore(combineReducers(val),
                            window.__REDUX_DEVTOOLS_EXTENSION__
                            && window.__REDUX_DEVTOOLS_EXTENSION__());
                    }
                }
            });

            return stor;
    },
    groupeMenu:(data)=>{
        let newData = [];
        data.forEach(item=>{
            if(item.IS_PARENT && item.DEPTH_LEVEL  === 1){
                item.children = data.filter(elem=>(elem.CHAIN[0] === item.CHAIN[0] && elem.DEPTH_LEVEL  > 1) );
                newData.push(item);
            }
            else if(item.DEPTH_LEVEL === 1) newData.push(item);
        });
        return newData;
    },

    downloadJSAtOnload: function(scripts) {
        var element;
        for (var i = 0; i < scripts.length; i++) {
            element = document.createElement("script");
            element.src = scripts[i];
            element.type = 'text/javascript';
            document.body.appendChild(element);

        }
    },

    loadCss: function(filename) {
        filename.forEach(item => {
            var l = document.createElement('link');
            l.rel = 'stylesheet';
            l.href = item;
            var h = document.getElementsByTagName('head')[0];
            h.parentNode.insertBefore(l, h);
        })

    }
}
