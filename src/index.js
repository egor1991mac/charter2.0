import React, {Suspense,lazy} from 'react';
import ReactDOM from 'react-dom';
import './style/style.css';
import Header from './layoute/header';
const App = lazy(() => import('./pages/index.jsx'));
const Stepper = lazy(() => import('./pages/stepper/index.js'));


if(document.getElementById('header')){
    ReactDOM.render(
        <Header />,
        document.getElementById('header'));
}


if( document.getElementById('travelsoft_search_form')){
        ReactDOM.render(
            <Suspense fallback={''}>
                <App />
            </Suspense>    
            ,
            document.getElementById('travelsoft_search_form'));

}

if( document.getElementById('travelsoft_stepper')){
    ReactDOM.render(
        <Suspense fallback={''}>
            <Stepper />
        </Suspense>
        ,
        document.getElementById('travelsoft_stepper'));

}



