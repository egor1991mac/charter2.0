import React, {Component} from 'react';
import Loader from '../component/loader';

export default function withLoading(Component) {
    return class withLoading extends Component {
        constructor(props) {
            super(props);
            this.state.loading = true;
        }



        async componentDidMount() {
            if (super.componentDidMount()) {
                await super.componentDidMount();
            }
            this.setState({loading: false});
        }



        render() {


            const {loading} = this.state;
            if (loading) {
                return <Loader/>
            } else return super.render();

        }
    }
}


//export default WithLoading;