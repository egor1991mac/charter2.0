import React, {Component} from 'react';
import {

    DEFAULT_PEOPLE,
    AIR_CLASS,
    MESSAGE_AVIA

} from "../config";
import {
    GET,

} from "../api";
import {pickBy, isEmpty, find, sortBy, groupBy, mapValues} from 'lodash';
import moment from 'moment';


const initial_data = (key, defaultData = null) => {
    if (sessionStorage.getItem(key) != "undefined" && sessionStorage.getItem(key) != null && sessionStorage.getItem(key) != undefined) {
        return JSON.parse(sessionStorage.getItem(key));
    } else {
        return defaultData;
    }
};

class withStepper extends Component {
    constructor(props) {
        super(props);
        this.initData = null;
    };

    state = {
        PEOPLE: DEFAULT_PEOPLE, // количество пассажиров
        AIR_CLASS,
        from: null,
        to: null,
        DATE_FROM: new Date(),
        DATE_TO: null,
        WAY: false
    };



    render() {
        return this.props.children(
            {
                ...this.state,

            })

    }
}

export default withStepper;
