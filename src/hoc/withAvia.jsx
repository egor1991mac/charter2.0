import React, {Component} from 'react';
import {

    DEFAULT_PEOPLE,
    AIR_CLASS,
    MESSAGE_AVIA

} from "../config";
import {
    GET,

} from "../api";
import {pickBy, isEmpty, find, sortBy, groupBy, mapValues} from 'lodash';
import moment from 'moment';


const initial_data = (key, defaultData = null) => {
    if (sessionStorage.getItem(key) != "undefined" && sessionStorage.getItem(key) != null && sessionStorage.getItem(key) != undefined) {
        return JSON.parse(sessionStorage.getItem(key));
    } else {
        return defaultData;
    }
};

class withAvia extends Component {
    constructor(props) {
        super(props);
        this.initData = null;
    };

    state = {
        PEOPLE: DEFAULT_PEOPLE, // количество пассажиров
        AIR_CLASS,
        from: null,
        to: null,
        DATE_FROM: new Date(),
        DATE_TO: null,
        WAY: false
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log(this.state);
    }

    getData = (data) => {
        GET(`https://autocomplete.travelpayouts.com/places2?term=${data}&locale=ru&types[]=city&types[]=airport`)
            .then(response => {
                this.initData = response;
                let data = groupBy(response, obj => obj.country_name);
                let fromKeys = Object.keys(data);
                fromKeys.forEach(item => {
                    if (item == "undefined") {
                        delete data[item];
                    }
                });
                this.setState({data});
            });
    };
    setDataAir = key => data => {

        this.setState({[key]: this.initData.find(item => item.name == data)});
    };
    getDate = (key) => (data) => {
        if (key == 'DATE_FROM') {
            this.setState({'DATE_TO': data});
        }
        this.setState({[key]: data});
    };
    search_avia = () => {
        const {from, to, DATE_FROM, DATE_TO, PEOPLE, AIR_CLASS, WAY} = this.state;


        if (WAY) {
            window.open(
                `http://bilet.tio.by/m/preloader/
            ${from.code}
            ${to.code}
            ${moment(DATE_FROM).format('DD.MM.YYYY')}0%7C
            ${DATE_TO != null ? moment(DATE_TO).format('DD.MM.YYYY') : moment(DATE_FROM).add(1, 'days').format('DD.MM.YYYY')}/
            ${PEOPLE.adults}%7C
            ${PEOPLE.children}%7C
            ${PEOPLE.baby}/
            ${Array.isArray(AIR_CLASS) ? AIR_CLASS[0].key : AIR_CLASS.key}`, "_blank");
        } else {
            window.open(
                `http://bilet.tio.by/m/preloader/
            ${from.code}
            ${to.code}
            ${moment(DATE_FROM).format('DD.MM.YYYY')}0/
            ${PEOPLE.adults}%7C
            ${PEOPLE.children}%7C
            ${PEOPLE.baby}/
            ${Array.isArray(AIR_CLASS) ? AIR_CLASS[0].key : AIR_CLASS.key}`, "_blank");
        }
    }

    render() {
        return this.props.children(
            {
                ...this.state,
                SET_DATA_AIR: this.setDataAir,
                GET_DATA: this.getData,
                GET_DATE: this.getDate,
                SEARCH_AVIA: this.search_avia,
                MESSAGE_AVIA
            })

    }
}

export default withAvia;
