import React, {Component} from 'react';
import {
    DEFAULT_CITY,
    DEFAULT_NIGHT,
    DEFAULT_PEOPLE,
    queryNew,
    CHARTER_URL,
    MESSAGE_CHARTER

} from "../config";
import json from './index.json';
import {
    POST_EACH,
    _FITLER_DATE,
    _UNION,
    _VALIDATION,
    SET_DATA, _FORMAT_DATE
} from "../api";
import axios from 'axios';
import {param} from 'jquery';

import {pickBy, isEmpty, find, sortBy, groupBy, isEqual} from 'lodash';
import qs from 'qs';


const initial_data = (key, defaultData = null) => {
    if (sessionStorage.getItem(key) != "undefined" && sessionStorage.getItem(key) != null && sessionStorage.getItem(key) != undefined) {
        return JSON.parse(sessionStorage.getItem(key));
    } else {
        return defaultData;
    }
};

class withCharter extends Component {
    constructor(props) {
        super(props);
        this.Tokens = undefined;
        this.Direction = this.props.data;
        this.body = [];
        console.log(this.props.data);
    };
    state = {
        loading: {message: '', status: false},
        NIGHT: DEFAULT_NIGHT.range, // количество ночей
        PEOPLE: DEFAULT_PEOPLE,
        DIRECTION_TO:undefined,
        statusHostName: [],
        way: initial_data('WAY') ? initial_data('WAY') : true,// количество пассажиров
    };

    async componentDidMount() {
        try {
            let auth = await  this.newAuthServer();

            auth.forEach(item=> item  && this.body.push(item));

            this.setStartData(this.props.data.directions.result);
            !initial_data('DIRECTION_FROM')
                ? this.findDefault(this.props.data.directions.result)
                : this.setState({
                        DIRECTION_FROM:initial_data('DIRECTION_FROM'),
                });

            initial_data('DIRECTION_TO') &&  this.setState({DIRECTION_TO:initial_data('DIRECTION_TO')});
            initial_data('DATE_FROM') &&  this.setState({DATE_FROM:initial_data('DATE_FROM')});
            initial_data('SELECTED_DATE_FROM') &&  this.setState({SELECTED_DATE_FROM:initial_data('SELECTED_DATE_FROM')});
            initial_data('PEOPLE') &&  this.setState({PEOPLE:initial_data('PEOPLE')});
            initial_data('NIGHT') &&  this.setState({NIGHT:initial_data('NIGHT')});

            if (window.location.pathname == CHARTER_URL) {

                this.getCharter2();
            }
        } catch (error) {
            console.log(error);
        }
    };
    

    componentDidUpdate(prevProps, prevState, snapshot) {
       if(!isEqual(prevState,this.state)){
           if(!isEqual(this.state.DIRECTION_FROM,prevState.DIRECTION_FROM)){
               this.setState({
                   DIRECTION_TO:undefined,
                   DATE_FROM:undefined,
                   SELECTED_DATE_FROM:undefined
                });
           }
           if(!isEmpty(this.state.DIRECTION_FROM) && isEqual(this.state.DIRECTION_TO,prevState.DIRECTION_TO)){
               this.getCityTo();
           }
           else{

            this.setState({
                DATE_FROM:undefined,
                SELECTED_DATE_FROM:undefined
             });
           }

           if(this.state.DIRECTION_TO && isEqual(this.state.DATE_FROM,prevState.DATE_FROM)){

                let date = [];
               console.log(this.state.DIRECTION_TO);
                if(this.state.DIRECTION_TO.data.hasOwnProperty('DATES')){
                    Object.keys(this.state.DIRECTION_TO.data.DATES).forEach(key=>{
                        this.state.DIRECTION_TO.data.DATES[key].forEach(elem=>{
                            if(Array.isArray(elem) && Object.keys(elem).length>1){
                               elem.forEach(item=>{
                                   date.push(item);
                               })
                            }
                            else{
                                date.push(elem);
                            }
                        })
                    })
               }
               else{
                   date.push([])
               }
               this.setState({DATE_FROM:[..._UNION(date)]});
               sessionStorage.setItem('DATE_FROM',JSON.stringify(_UNION(date)));
           }
           if(!isEmpty(this.state.DIRECTION_TO) && !isEmpty(this.state.SELECTED_DATE_FROM) ){
               const {DIRECTION_FROM, DIRECTION_TO} = this.state;
               const {operators} = this.Direction;
               let body = [];

               Object.keys(operators.result).forEach(indexOperator =>{
                   let from;
                   let to;
                    Object.keys(operators.result[indexOperator].ITEMS).forEach((item,index)=>{
                        from = DIRECTION_FROM.key == 'country'
                            ? {CountryFromKey: operators.result[indexOperator].ITEMS.COUNTRIES[DIRECTION_FROM.data.ID] && parseInt(operators.result[indexOperator].ITEMS.COUNTRIES[DIRECTION_FROM.data.ID].UF_OPERATOR)}
                            : {CityFromKey: operators.result[indexOperator].ITEMS.CITIES[DIRECTION_FROM.data.ID] && parseInt(operators.result[indexOperator].ITEMS.CITIES[DIRECTION_FROM.data.ID].UF_OPERATOR)};


                        to = DIRECTION_TO.key == 'country'
                            ? {CountryToKey: operators.result[indexOperator].ITEMS.COUNTRIES[DIRECTION_TO.data.ID] && parseInt(operators.result[indexOperator].ITEMS.COUNTRIES[DIRECTION_TO.data.ID].UF_OPERATOR)}
                            : {CityToKey: operators.result[indexOperator].ITEMS.CITIES[DIRECTION_TO.data.ID] && parseInt(operators.result[indexOperator].ITEMS.CITIES[DIRECTION_TO.data.ID].UF_OPERATOR)};

                    })
                        body.push({
                            searchFilter:{
                            CharterClass: 89,
                            ...from,
                            ...to,
                            },
                            link:`${operators.result[indexOperator].UF_CHARTER_GATEWAY}${!this.state.way ? 'GetFlights' : 'GetPeriodFlights' }`,
                            logo:operators.result[indexOperator].UF_LOGO,
                            name:operators.result[indexOperator].UF_NAME,
                            id:indexOperator
                        })

               })
               this.body.forEach((item,index)=>{
                   item.searchFilter = body[index].searchFilter;
                   item.link = body[index].link;
                   item.logo = body[index].logo;
                   item.name = body[index].name;
                   item.id = body[index].id;
               })
           }
       }
    }

   newAuthServer = (data) =>{
        const {operators} = this.props.data;
        let query= [];

        Object.keys(operators.result).forEach(item=>{
            query.push(
                {
                    AUTH: operators.result[item].AUTH,
                    link: `${operators.result[item].UF_CHARTER_GATEWAY}GetToken`
                });
        });
        
        return Promise.all(query.map(async item => {
            try {
                let result = await axios.post(item.link, {login:item.AUTH.LOGIN,password:item.AUTH.PASS});

                return result.data;
            }
            catch (e) {
                console.log(e);
            }

        }))
    }
    
    selectDirection = (defData=this.Direction.directions.result,key) => (data) =>{
        console.log();
        if(data.hasOwnProperty('country')){
            this.setState({[key]:{
                data:defData[data.country],
                    key:'country'
                }});
            sessionStorage.setItem(key,JSON.stringify({data:defData[data.country],key:'country'}))
        }
        else{
           Object.keys(defData).forEach(cityID =>{
                 if(defData[cityID].CITIES[data.city]){
                    this.setState({
                         [key]:{
                             data:defData[cityID].CITIES[data.city],
                             key:'city'
                         }
                     });
                     sessionStorage.setItem(key,JSON.stringify({
                        data:defData[cityID].CITIES[data.city],
                        key:'city'
                    }))
                }
            }
           )
        }
    };

    findDefault = (data,callback) =>{
        Object.keys(data).forEach(id=>{
            Object.keys(data[id]).forEach(property=>{
                if(data[id][property] == '21'){
                    Object.keys(data[id].CITIES).forEach(item=>{
                        if(data[id].CITIES[item].ID == '1028'){
                            this.setState({DIRECTION_FROM:{data:data[id].CITIES[item],key:'city'}});
                        }
                    })
                }
            })
        })
    }

    setStartData = (data) => {
        let tree =[];
        Object.keys(data).forEach(id=>{
            let children = [];
            let city = data[id].CITIES;
            Object.keys(city).forEach(cityID=>{
                children.push({
                    title: city[cityID].UF_NAME,
                    key:city[cityID].ID,
                    dataRef: 'city'
                })
            });
            tree.push({
                title:data[id].UF_NAME,
                key:data[id].ID,
                dataRef:'country',
                children: children
            });
        });
        this.setState((props,state)=>{
            return {
                FROM: tree
            }
        });
    };
    getCityTo = (defData) => {
        const {DEFAULT_FROM, FROM,DIRECTION_FROM} = this.state;
            let data = DIRECTION_FROM.data.DIRECTIONS;
            let tree = [];
            Object.keys(data).forEach(id=>{
                let children = [];
                let city = data[id].CITIES;
                Object.keys(city).forEach(cityID=>{
                    children.push({
                        title: city[cityID].UF_NAME,
                        key:city[cityID].ID,
                        dataRef: 'city'
                    })
                });
                tree.push({
                    title:data[id].UF_NAME,
                    key:data[id].ID,
                    dataRef:'country',
                    children: children
                });
            });
            this.setState((props,state)=>{
                return {
                    TO: tree,
                }
            });
            sessionStorage.setItem('FROM', JSON.stringify({data: this.state.DIRECTION_FROM, default: this.state.DIRECTION_FROM ? this.state.DIRECTION_FROM : defData}));    
    };
    getDateFrom = (key) => {
        const {DEFAULT_FROM, FROM, TO, DEFAULT_TO} = this.state;
        const from = DEFAULT_FROM.hasOwnProperty('city') ? {cityFromKey: DEFAULT_FROM.city.cityKey} : {countryFromKey: DEFAULT_FROM.country.countryKey};
        const body = (key != undefined || DEFAULT_TO != null)
            ? key.hasOwnProperty('city')
                ? {cityToKey: key.city, ...from}
                : {countryToKey: key.country, ...from}
            : DEFAULT_TO.hasOwnProperty('city')
                ? {cityToKey: DEFAULT_TO.city.cityKey, ...from}
                : {countryToKey: DEFAULT_TO.country.countryKey, ...from};

        body &&
        this.setState({loading: {message: MESSAGE_CHARTER.date.loading, status: true}});
        return SET_DATA(POST_EACH(queryNew, 'GET_DATE_FROM', this.Tokens, body, MESSAGE_CHARTER.date.to), queryNew)
            .then(response => {
                this.setState({loading: {message: '', status: false}});
                _VALIDATION(response,
                    (data) => {
                        this.setState({statusHostName: Object.keys(data)});
                        data = _UNION(data);
                        if (data) {
                            let defaultData = body.hasOwnProperty('cityToKey')
                                ? {city: TO.find(item => item.cityKey == body.cityToKey)}
                                : {country: TO.find(item => item.countryKey == body.countryToKey)};
                            this.setState({
                                DATE_FROM: data,
                                DEFAULT_TO: defaultData,
                                SELECTED_DATE_FROM: null,

                            });
                            sessionStorage.setItem('DATE_FROM', JSON.stringify({data: data, default: null}));
                            sessionStorage.setItem('TO', JSON.stringify({data: TO, default: defaultData}));
                        }
                    },
                    (data) => {
                        this.props.GET_STATUS({status: data.status, data: MESSAGE_CHARTER.date.warning});
                    },
                    (data) => {
                        this.props.GET_STATUS(data);
                    },
                )
            })
    };


    getData = (key) => (data) => {

        if (data) {
            this.setState({[key]: data});
            sessionStorage.setItem([key], JSON.stringify(data));
        }

    };
    changeWay = () => {
        this.setState({
            way: !this.state.way,
        });
        sessionStorage.setItem('WAY', JSON.stringify(!this.state.way));
    };
    getCharter2 = async () => {

        if (window.location.pathname != CHARTER_URL) {
            window.location.href = `${CHARTER_URL}`;
        }
        else{
            const {DATE_FROM,SELECTED_DATE_FROM,PEOPLE,NIGHT, DIRECTION_TO, DIRECTION_FROM} = this.state;
            let body;
            if(!this.state.way){
                body = [];
                _FITLER_DATE(DATE_FROM, SELECTED_DATE_FROM).forEach(item=>{
                    this.body.forEach(host=>{
                        console.log(host);
                        !Object.values(host.searchFilter).includes(undefined) &&
                        body.push({
                            token:host.token,
                            searchFilter:{
                                ...host.searchFilter,
                                TourDate: item,
                                RateISOCode: "BYN",
                                CharterClass: 89,
                                count: [PEOPLE.adults, PEOPLE.children]
                            },
                            link:host.link,
                            name:host.name,
                            logo:host.logo,
                            id:host.id
                        })
                    })
                })
            }
            else{
                body = [];
                this.body.forEach(host=>{
                    !Object.values(host.searchFilter).includes(undefined) &&
                    body.push({
                        token:host.token,
                        SearchPeriodFilter:{
                            ...host.searchFilter,
                            DurationFrom: NIGHT[0],
                            DurationTo: NIGHT[1],
                            PlacesCountBack: PEOPLE.adults + PEOPLE.children,
                            PlacesCountTo: PEOPLE.adults + PEOPLE.children,
                            RateISOCode: "BYN",
                            TourDates: _FITLER_DATE(DATE_FROM, SELECTED_DATE_FROM),
                            count: [PEOPLE.adults, PEOPLE.children]
                        },
                        link:host.link,
                        name:host.name,
                        logo:host.logo,
                        id:host.id
                    })
                })
            };
            const from = DIRECTION_FROM.hasOwnProperty('city') ? {CityFromKey: DIRECTION_FROM.data.ID} : {CountryFromKey: DIRECTION_FROM.data.ID};
            const to = DIRECTION_TO.hasOwnProperty('city') ? {CityToKey: DIRECTION_TO.data.ID} : {CountryToKey:DIRECTION_TO.data.ID};

            body = qs.stringify(body,{arrayFormat :  ' brackets '});

            body &&
            this.setState({loading: {message: MESSAGE_CHARTER.charter.loading, status: true}});
            axios.post('https://www.tio.by/ajax/getCharterTours.php',JSON.stringify('data',{operators:body,all:{
                SearchPeriodFilter:{
                     ...from,
                    ...to,
                    DurationFrom: NIGHT[0],
                    DurationTo: NIGHT[1],
                    PlacesCountBack: PEOPLE.adults + PEOPLE.children,
                    PlacesCountTo: PEOPLE.adults + PEOPLE.children,
                    RateISOCode: "BYN",
                    TourDates: _FITLER_DATE(DATE_FROM, SELECTED_DATE_FROM),
                    count: [PEOPLE.adults, PEOPLE.children]
                },
            }}));

            Promise.all(body.map(async item => {
                try {
                    let data = !this.state.way ? {searchFilter:item.searchFilter} : {SearchPeriodFilter:item.SearchPeriodFilter};
                    let response = await axios.post(item.link, {token: item.token, ...data});
                    this.setState({loading: {message: MESSAGE_CHARTER.charter.loading, status: true}});
                    return await response.data.length != 0 ? {status:'success',data:{data:response.data,
                            host:{link:item.link,
                                name:item.name,
                                logo:item.logo}}} : {status:'warning',data:response.data}
                } catch (e) {
                    return {status:'error',message:'Error'}
                }
            }))
                .then(resp=>{
                    this.setState({loading: {message: '', status: false}});
                    _VALIDATION(resp,
                        data=>{
                            let newbuf = [];
                            let buf = [];
                            Object.values(data).forEach(({data})=>data.data.forEach(elem=>{
                                elem.host={
                                    logo:data.host.logo,
                                    name:data.host.name,
                                    link:data.host.link
                                };
                                newbuf.push(elem);

                            }));

                            console.log(newbuf);
                            newbuf= groupBy(newbuf, obj => obj.cityFromCode + '/' + obj.cityToCode + '/' + obj.dateTo + '/' + obj.dateBack);

                            Object.keys(newbuf).forEach(item=>{
                                buf.push({item:newbuf[item]});
                            });

                            this.props.GET_DATA(
                                this.state.way,
                                {
                                    data: buf,
                                    body:
                                        {
                                            searchFilter:
                                                {
                                                    count: [PEOPLE.adults, PEOPLE.children]
                                                }
                                        }
                                })
                        },
                        (data) => {
                            this.props.GET_STATUS({status: 'warning', data: MESSAGE_CHARTER.charter.warning});
                        },
                        (data) => {
                            this.props.GET_STATUS({status: 'error', data: MESSAGE_CHARTER.charter.warning});
                        },
                    )
                })
        }

    };
    
    getCharter = () => {
        if (window.location.pathname != CHARTER_URL) {
            window.location.href = `${CHARTER_URL}`;
        }
    
        const {DATE_FROM, SELECTED_DATE_FROM, NIGHT, PEOPLE, DEFAULT_FROM, DEFAULT_TO} = this.state;
        const from = DEFAULT_FROM.hasOwnProperty('city') ? {CityFromKey: DEFAULT_FROM.city.cityKey} : {CountryFromKey: DEFAULT_FROM.country.countryKey};
        const to = DEFAULT_TO.hasOwnProperty('city') ? {CityToKey: DEFAULT_TO.city.cityKey} : {CountryToKey: DEFAULT_TO.country.countryKey};
        const {way} = this.state;

        let body = way ? {
                SearchPeriodFilter: {
                    CharterClass: 89,
                    ...from,
                    ...to,
                    DurationFrom: NIGHT[0],
                    DurationTo: NIGHT[1],
                    PlacesCountBack: PEOPLE.adults + PEOPLE.children,
                    PlacesCountTo: PEOPLE.adults + PEOPLE.children,
                    RateISOCode: "BYN",
                    TourDates: _FITLER_DATE(DATE_FROM, SELECTED_DATE_FROM),
                    count: [PEOPLE.adults, PEOPLE.children]
                }
            }
            : {
                searchFilter: {
                    ...from,
                    ...to,
                    TourDate: _FORMAT_DATE(SELECTED_DATE_FROM, 'YYYY-MM-DD').from,
                    RateISOCode: "BYN",
                    CharterClass: 89,
                    count: [PEOPLE.adults, PEOPLE.children]
                }
            };
        if (body) {
            this.setState({loading: {message: MESSAGE_CHARTER.charter.loading, status: true}});
            return SET_DATA(POST_EACH(queryNew, way ? 'GET_CHARTER' : 'GET_CHARTER_ONE_WAY', this.Tokens, body, MESSAGE_CHARTER.charter.error, this.state.statusHostName),
                this.state.statusHostName.length > 0 ? this.state.statusHostName : queryNew)
                .then(response => {
                    this.setState({loading: {message: '', status: false}});
                    _VALIDATION(response,
                        (data) => {
                            let result = [];
                            Object.keys(data).forEach(host => {
                                data[host].data.forEach(item => {
                                    item.host = {name: host, img: require(`../img/${host}.png`)};
                                    result.push(item);
                                });
                            });
                            let newResult = [];
                            let newData = groupBy(result, obj => obj.cityFromCode + '/' + obj.cityToCode + '/' + obj.dateTo + '/' + obj.dateBack);
                            Object.keys(newData).forEach(keys => {
                                newResult.push({[keys]: newData[keys]});
                            });
                            this.props.GET_DATA(this.state.way, {data: newResult, body});

                        },
                        (data) => {
                            this.props.GET_STATUS({status: data.status, data: MESSAGE_CHARTER.charter.warning});
                        },
                        (data) => {
                            this.props.GET_STATUS(data);
                        },
                    )
                })
        }
    };
    render() {
        return this.props.children(
            {
                ...this.state,
                SELECT_DIRECTION: this.selectDirection,
                //SELECT_DIRECTION2: this.selectDirection2,
                GET_CITY_FROM: this.getCityFrom,
                GET_CITY_TO: this.getCityTo,
                GET_DATE_FROM: this.getDateFrom,
                GET_DATA: this.getData,
                GET_CHARTER: this.getCharter,
                DEFAULT_NIGHT,
                DEFAULT_PEOPLE,
                MESSAGE_CHARTER,
                CHANGE_WAY: this.changeWay,
                GET_CHARTER2: this.getCharter2
            })

    }
}

export default withCharter;
