import React, {Component, Suspense, lazy} from 'react';
import {isEmpty} from 'lodash';
import Alert from '../component/alert';



export default class withStateData extends Component {
    constructor(props) {
        super(props);
    };

    state = {
        close: false
    };

    componentDidMount() {
        let portal = document.createElement('div');
        portal.id = 'portal';
        document.body.appendChild(portal);
    }

    closeAlert = () => {
        this.setState({
            close: !this.state.close,
            type:undefined,
            message:undefined
        });
    };




    getStatus = (data) => {
         if(!isEmpty(data)) {
             this.setState({
                 type: data.status,
                 message: data.data,
                 close:false
             })
         }
    };


    render() {

        const {message, type, close} = this.state;


        return <>
            {
                this.props.children({...this.state, GET_STATUS: this.getStatus})
            }
            <div>
                {
                    type != undefined && <Alert message={message} type={type} close={close} onClose={this.closeAlert}/>
                }
            </div>
        </>
    }
}
