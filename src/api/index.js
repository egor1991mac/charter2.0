import axios from 'axios';
//import convert from 'xml-to-json-promise';
//import co from 'co';

import {difference, unionBy,isEmpty, find, pickBy, mapValues, merge} from 'lodash';
import moment from "moment";





const _CREATE_OBJECT = (query) =>{
    let data={};
    for(let key in query){
        data[key] = {
            status:'',
            data:[]
        }
    }
    return data;
};

const _GET_EACH = async (query, url, auth,body, message,callback) => {
    let data = {LOADING:{message:'Идет загрузка',state :true}, DATA:[]};
    if(callback){
        callback(data);
    }
    return new Promise((resolve) => {
            query.forEach((item, index) => {
                    axios.post(item[url], Array.isArray(auth) ? { ...auth[index].SUCCESS,...body} : item.AUTH , {timeout:10000})
                        .then(response => response.data.length != 0 ? data.DATA.push({'SUCCESS': response.data}) : data.DATA.push({'WARNING':message })
                        )
                        .then(response => {
                            if (data.DATA.length == query.length) {
                                if(callback){
                                    data.LOADING = {message:'Загрузка завершена',state :false};
                                    callback(data);
                                }
                                resolve(data.DATA);
                            }
                        }).catch(error => {
                            data.DATA.push({'ERROR': error});
                        if (data.DATA.length == query.length) {
                            if (data.DATA.length == query.length) {
                                if (callback) {
                                    data.LOADING = {message: 'Загрузка завершена', state: false};
                                    callback(data);
                                }
                                resolve(data.DATA);
                            }
                        }}
                        )
                }
            );
        }
    )
};
const SET_DATA = (promise,keys) => new Promise(resolve => {
    let data ={};
    promise.then(resp=>{
        resp.forEach(item=>{
            item.then(res=>{
                data[Object.keys(res)] = res[Object.keys(res)];
                if(Object.keys(data).length == Object.keys(keys).length){
                    resolve(data);
                }
            })
        })
    })

});

const GET = (url,header) => axios.get(url).then(response=>response.data);


const POST_EACH = (query, url, auth,body,ErrorMessage,statusHostName=[]) => new Promise( resolve =>{
    if(statusHostName.length == 0){
    let result = Object.keys(query).map(async host=>{
        let AUTH = (auth == undefined) ? query[host].AUTH : auth[host].status != 'error' ? auth[host].data: false;
          if(AUTH != false){
              const httpPost = axios.create();
              httpPost.defaults.timeout = 25000;
              return httpPost.post(
                  query[host][url],
                  {...AUTH,...body}
                  )
                  .then(result => {
                      return {[host]: result.data.length == 0 ? {status:'warning',data:result.data} : {status:'success',data:result.data}}})
                  .catch(e => {return {[host]:{status:'error',data:ErrorMessage}}})
          }
          else return {[host]:{status:'error',data:'ошибка авторизации'}}
         });

            return resolve(result);
    }
        else{
        let result = statusHostName.map(async host=>{
            let AUTH = (auth == undefined) ? query[host].AUTH : auth[host].status != 'error' ? auth[host].data: false;

            if(AUTH != false){
                const httpPost = axios.create();
                httpPost.defaults.timeout = 25000;
                return httpPost.post(
                    query[host][url],
                    {...AUTH,...body}
                )
                    .then(result => {
                        return {[host]: result.data.length == 0 ? {status:'warning',data:result.data} : {status:'success',data:result.data}}})
                    .catch(e => {return {[host]:{status:'error',data:ErrorMessage}}})
            }
            else return {[host]:{status:'error',data:'ошибка авторизации'}}
        });

        return resolve(result);
    }


}
);



const _UNION = (data, by) => {

    let buffer = [];
    let result =[];
    if(Array.isArray(data)){
        data.forEach(item => buffer.push(item));
         by ? unionBy(buffer, by) : unionBy(buffer);
    }
    else {
        for(let key in data){
            data[key].data.forEach(item=>{
                buffer.push(item);
            });
            by ? unionBy(buffer,by) : unionBy(buffer);
        }

    }
    return buffer;
};

const _GET = async (URL, CONFIG, BODY, ERROR_MESSAGE, WARNING_MESSAGE) => {
    try {
        let response = await fetch(URL, CONFIG !== null ? CONFIG : {
            method: 'POST',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(BODY)
        });

        response = await response.json();

        if (await response.length == 0) {
            return {WARNING: WARNING_MESSAGE}
        } else
            return await {SUCCESS: response};

    } catch (e) {
        return {ERROR: ERROR_MESSAGE}
    }
};
const _FORMAT_DATE = (data, format) => {
    if (Array.isArray(data)) {
        return data.map(item => moment(item).format(format));
    } else if (Object.keys(data).length > 0) {
        for (let key in data) {
            data[key] = moment(data[key]).format(format);
        }
        return data;
    }
};
const _FITLER_DATE = (dates, range) => {
    if (range) {
        range = _FORMAT_DATE(range, 'YYYY-MM-DD');
        dates = dates.filter(date => {
            if (date >= range.from && date <= range.to) {
                return date;
            }
        });
        return dates;
    } else {
        return dates;
    }
};
const _CREATE_DATE = (days) => {
    let result = [];
    for (let i = 0; i < days; i++) {
        result.push(moment(new Date()).add(i, 'day').format('YYYY,M,DD'));
    }
    return result;
};
const _DIFERENCE_DATE = (date1, date2) => {
    return difference(date1, date2).map(item => new Date(item));
};

const _VALIDATION = (data,success,warning,error) =>{
    let result = {};
    result = pickBy(data, obj=> obj.status=="success" && obj.data != undefined);
    if(!isEmpty(result)){
        success(result);
    }
    else {
        result = pickBy(data, obj=> obj.status=="warning");

        if(!isEmpty(result)){
            warning(result[Object.keys(result)[0]]);
        }
        else{
            result = pickBy(data, obj=> obj.status=="error");
            error(result[Object.keys(result)[Object.keys(result).length-1]]);
        }
    }
};
const _VALIDATION_XHR_DATA = async (data,success, warning, error) => {

    for (let key in data[0]) {
        switch (key) {
            case 'SUCCESS':
                success();
                break;
            case 'WARNING':
                warning();
                break;
            case 'ERROR':
                error();
                break;
        }
    }
};

export {_GET, _FORMAT_DATE, _CREATE_DATE, _DIFERENCE_DATE, _FITLER_DATE, _VALIDATION_XHR_DATA, _GET_EACH, GET, _UNION, _CREATE_OBJECT,POST_EACH, SET_DATA, _VALIDATION};