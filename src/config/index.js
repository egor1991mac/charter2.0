// методы
const queryNew = window.charterQuery;
const PATH = window.charterPath;
const CHARTER_URL = window.charterUrl;

const QUERY =
  [{
            AUTH: {LOGIN: 'travelsoft', PASSWORD: 'hdyNb81Uf-q&51M'},
            GET_TOKEN: 'http://online.abstour.by:88/ChartersWin64/api/Charter/GetToken',
            GET_FROM_CITY: 'http://online.abstour.by:88/ChartersWin64/api/Charter/GetDepartureCities',
            GET_TO_CITY: 'http://online.abstour.by:88/ChartersWin64/api/Charter/GetArrivalCities',
            GET_DATE_FROM: 'http://online.abstour.by:88/ChartersWin64/api/Charter/GetStraightFlightDates',
            GET_CHARTER: 'http://online.abstour.by:88/ChartersWin64/api/Charter/GetPeriodFlights'
}];
const {MESSAGE_CHARTER , MESSAGE_AVIA, MESSAGE_TABS} = window.FormMessage;

//стандартный город вылета
const DEFAULT_CITY = {airportCode: 'MSQ'};
const DEFAULT_NIGHT = {
    range: [6, 14]
};


const DEFAULT_PEOPLE = {
    adults: 1,
    children: 0,
    baby: 0
};

const AIR_CLASS = [
    {
        name:'Любой',
        key:'A'
    },
    {
        name:'Эконом',
        key:'E'
    },
    {
        name:'Бизнес',
        key:'B'
    }
]

export {
    DEFAULT_CITY,
    DEFAULT_NIGHT,
    DEFAULT_PEOPLE,
    queryNew,
    CHARTER_URL,
    AIR_CLASS,
    MESSAGE_CHARTER,
    MESSAGE_AVIA,
    MESSAGE_TABS
}
