import {Tree} from 'antd';
import React, {Component} from 'react';
import {groupBy, unionBy} from 'lodash';
//import styles from "../radio/style.module.css";
const {TreeNode} = Tree;

const createTree = (props,key) =>{
    const data = groupBy(props, key);
    let tree = [];
    Object.keys(data).forEach(country => {
        data[country].forEach(elem => {
            tree.push({
                title: country,
                key: data[country][0].countryKey,
                dataRef: 'country',
                children: unionBy(data[country], 'cityName').map(item => {
                    return {
                        title: `${item.cityName}`,
                        key: item.cityKey,
                        dataRef: 'city'
                    }
                })
            })
        })
    });
    return tree;
};

class tree extends Component {
    constructor(props){
        super(props);
        this.state = { data : this.props.DATA};
    }
    componentWillReceiveProps(nextProps, nextContext) {

        let tree = createTree(nextProps.DATA,'countryName');
        this.setState({tree: unionBy(tree, 'key')});
    }

    componentWillMount() {

        let tree = createTree(this.props.DATA,'countryName');
        this.setState({tree: unionBy(tree, 'key')});
    }

    renderTreeNodes = data => data.map((item) => {
        if (item.children) {
            return (
                <TreeNode title={item.title} key={item.key} dataRef={item.dataRef}>
                    {this.renderTreeNodes(item.children)}
                </TreeNode>
            );
        }
        return <TreeNode {...item} />;
    });

    handleSelect = (selectKeys, e) => {

        if (selectKeys.length != 0) {
            e.node.props.dataRef == 'country' ?
                this.props.GET({country: selectKeys[0]})
                : this.props.GET({city: selectKeys[0]})
        }

    };

    render() {
        const {tree} = this.state;

        if (tree) {
            return <Tree
                autoExpandParent
                defaultSelectedKeys={this.props.DEFAULT_DATA}
                onSelect={this.handleSelect}
                defaultExpandedKeys={this.props.DEFAULT_DATA == null ? undefined : this.props.DEFAULT_DATA}
            >
                {this.renderTreeNodes(tree)}
            </Tree>
        } else {
            return ''
        }

    }
}

export default tree;