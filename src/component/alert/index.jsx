import React from 'react';
import ReactDOM from 'react-dom';
import style from './style.module.css';
import {none} from '../../style/display.module.css';

class Index extends React.Component {
    constructor(props) {
        super(props);
        this.ref = React.createRef();
    }
    componentDidMount() {
        let alertBody = document.createElement('div');
        alertBody.id = '#portal';
        document.body.appendChild(alertBody);

        this.ref.current.onclick = (e)=>{
            this.ref.current.children[0] !=  e.target && this.props.onClose();
        }
    }

    render() {

        const {message, type, close, onClose} = this.props;
        return ReactDOM.createPortal(
            <div ref={this.ref} className={close == false ? style.alert : none}>
                <div className={style.body}>
                    {message}
                </div>
            </div>,
            document.querySelector('#portal'))
    }
}

export default Index;

