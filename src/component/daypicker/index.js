import React from 'react';
import DayPicker, {DateUtils} from 'react-day-picker';
import MomentLocaleUtils from 'react-day-picker/moment';
import  'moment/locale/ru';
import 'react-day-picker/lib/style.css';
import {_FORMAT_DATE, _CREATE_DATE, _DIFERENCE_DATE} from '../../api';
import style from './style.module.css';
import {isEqual} from 'lodash';
import './style.css';

class Calendar extends React.PureComponent {
    static defaultProps = {
        numberOfMonths: window.innerWidth < 768 ? 1 : 2
    };

    constructor(props) {
        super(props);
        this.handleDayClick = this.handleDayClick.bind(this);
        this.handleResetClick = this.handleResetClick.bind(this);
        this.state = this.getInitialState();
    }

    componentWillReceiveProps(nextProps, nextContext) {


        this.setState({
            from: nextProps.DEFAULT_DATA ? nextProps.DEFAULT_DATA.from : null,
            to: nextProps.DEFAULT_DATA ? nextProps.DEFAULT_DATA.to : null,
            selectOneDay: nextProps.DEFAULT_DATA && nextProps.DEFAULT_DATA
        })


    }


    getInitialState() {
        if(!this.props.def){
            return {
                from: this.props.DEFAULT_DATA && this.props.DEFAULT_DATA.from ,
                to: this.props.DEFAULT_DATA && this.props.DEFAULT_DATA.to,
                selectOneDay: this.props.DEFAULT_DATA && this.props.DEFAULT_DATA
            };
        }
        else{
            return {
                selectOneDay:  this.props.DEFAULT_DATA && this.props.DEFAULT_DATA
            }
        }
    };

    formatDisableDate = () => {
        let result = _DIFERENCE_DATE(_CREATE_DATE(360), _FORMAT_DATE(this.props.DATA, 'YYYY,M,DD'));
        result.push({before: new Date()});
        return result;
    };


    handleDayClick(day) {
        if(!this.props.def){
            if(this.props.multiple){
                const range = DateUtils.addDayToRange(day, this.state);
                this.setState(range);
                if(range.to == null){
                    range.to = range.from;
                }
                this.props.GET_DATA({...range});
            }
            else{
                this.setState({selectOneDay:day});
                this.props.GET_DATA({from:day,to:day});
            }
        }
        else{
            this.setState({selectOneDay:day});
            this.props.GET_DATA(day);
        }
    }

    handleResetClick() {
        this.setState(this.getInitialState());
    }
    render() {
        const {def, MINDATE, multiple} = this.props;
        if(!def){
            const {from, to} = this.state;
            const modifiers = {start: from, end: to};

            return (
                <DayPicker
                    className={style.DayPicker}
                    fromMonth = {new Date()}
                    numberOfMonths={this.props.numberOfMonths}
                    //month={new Date(this.props.DATA ? _FORMAT_DATE(this.props.DATA,'YYYY,M,DD')[0] : null)}
                    selectedDays={
                        multiple ? [from, {from, to}] : this.state.selectOneDay }
                    modifiers={modifiers}
                    disabledDays={this.formatDisableDate()}
                    onDayClick={this.handleDayClick}
                    localeUtils={MomentLocaleUtils} locale={'ru'}
                />
            );
        }
        else{
            const {selectOneDay} = this.state;
            return <DayPicker
                className={style.DayPicker}
                fromMonth = {new Date()}
                disabledDays = {{
                    before: MINDATE
                }}
                numberOfMonths={this.props.numberOfMonths}
                onDayClick={this.handleDayClick}
                localeUtils={MomentLocaleUtils} locale={'ru'}
                selectedDays={ selectOneDay }
            />
        }

    }
}


export default Calendar;

