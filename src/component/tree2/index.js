import {Tree} from 'antd';
import React, {Component} from 'react';
import {groupBy, unionBy} from 'lodash';
//import styles from "../radio/style.module.css";
const {TreeNode} = Tree;

const createTree = (data,key) =>{


    //return this.props.DATA;
};

class tree extends Component {
    constructor(props){
        super(props);
        this.state = { data : this.props.DATA};

    }
   
    renderTreeNodes = data => data.map((item) => {
        if (item.children) {
            return (
                <TreeNode title={item.title} key={item.key} dataRef={item.dataRef}>
                    {this.renderTreeNodes(item.children)}
                </TreeNode>
            );
        }
        return <TreeNode {...item} />;
    });

    handleSelect = (selectKeys, e) => {
        if (selectKeys.length != 0) {
            e.node.props.dataRef == 'country' ?
                this.props.GET({country: selectKeys[0]})
                : this.props.GET({city: selectKeys[0]})
        }

    };

    render() {
        const {DATA} = this.props;
        
        if (DATA) {
            return <Tree
                autoExpandParent
                defaultSelectedKeys={this.props.DEFAULT_DATA && [this.props.DEFAULT_DATA]}
                onSelect={this.handleSelect}
                defaultExpandedKeys={this.props.DEFAULT_DATA && [this.props.DEFAULT_DATA]}
            >
                {this.renderTreeNodes(DATA)}
            </Tree>
        } else {
            return ''
        }

    }
}

export default tree;