import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Pagination} from 'antd';
import {chunk} from "lodash";
import style from './style.module.css';
import grid from '../../style/grid.module.css';
import flex from '../../style/flex.module.css';
import indents from '../../style/indents.module.css';
class PaginationData extends Component {
    constructor(props){
        super(props);
        this.ref = React.createRef();
    }
   state = {
       data:[],
       pages:0
   };
   componentWillReceiveProps(nextProps, nextContext) {
       if(nextProps.data != null){
           console.log(nextProps.data);
           this.setState({data: chunk(nextProps.data,10)})
       }
   }
    selectPage = value =>{
        this.setState({pages:value-1});
        window.scrollTo({
            top:0,
            behavior: "smooth"
        });
    };


    render() {
       const {data,pages} = this.state;

        if(data.length > 0) return ReactDOM.createPortal(
            <div ref = {this.ref} className={`${grid.wrap} ${indents.pt__2}`}>
                <>
                {
                    React.cloneElement(this.props.children,{...this.state, data: data[pages], count: this.props.count},)
                }
                </>
                <div className={`${grid.row} ${flex.justify_content__center} ${flex.justify_content_lg__flex_end}`}>
                    {
                        <Pagination
                            onChange={this.selectPage}
                            pageSize={10}
                            total={this.props.data && this.props.data.length}
                            className={style.pagination}
                        />
                    }

                </div>
            </div>
        , document.querySelector('#result-charter'));
        else return ''
    }
}

export default PaginationData;