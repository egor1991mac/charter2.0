import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Collapse from '@material-ui/core/Collapse';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {connect} from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';

const styles = theme =>({
    list: {
        width: 270,
    },
    CollapselistItem: {
        paddingLeft: 0,
    },
    CollapseListText:{
        paddingLeft: `${theme.spacing(4)}px !important`,
    },
    menuIconColor:{
            fill:'#212121',

    },
    menuIcon:{
      marginLeft:'auto !important',
        padding:'0px !important'
    },

    fullList: {
        width: 'auto',
    },
    searchContainer:{
        background:'rgba(236, 236, 236, 0.42)',
        padding:'0.5rem 0',
        marginTop:theme.spacing.unit,
    },
    Divider:{
        marginTop:16,

    },
    heading:{
        margin:'16px 16px 0',
    }
});

class TemporaryDrawer extends React.Component {
    state = {
        open: false
    };

    toggleDrawer = (side, open) => () => {
        this.setState({
            [side]: open,
        });
    };
    handleClick = () => {
        this.setState(state => ({ open: !state.open }));
    };


    render() {
        const { classes, Links, Open, OpenMobileMenu, SelectNav, children } = this.props;

        const sideList = (
            <div className={classes.list}>
                <div className={classes.heading}>
                    <Typography variant={"h6"}>
                        TIO.BY
                    </Typography>
                    <Typography variant={"caption"}>
                        Информационно туристический портал
                    </Typography>
                </div>
                <div className={classes.searchContainer}>
                    {children}
                </div>

                <List>

                    {
                    Links.map((item) => (
                        !item.hasOwnProperty('children')
                            ?
                                <ListItem button key={Math.random()} button={true} onClick={SelectNav.bind(this,item.LINK)}>
                                    <ListItemText primary={item.TEXT}/>
                                </ListItem>
                            :
                                <span key={Math.random()}>
                                <ListItem key={Math.random()} button onClick={this.handleClick}>
                                    <ListItemText  primary={item.TEXT} />
                                    {this.state.open ? <ExpandLess /> : <ExpandMore />}
                                </ListItem>
                                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                                    <List component="div" disablePadding >
                                        {
                                            item.children.map(children=>
                                                    <ListItem key={Math.random()} button className={classes.CollapselistItem} onClick={SelectNav.bind(this,children.LINK)}>
                                                        <ListItemText className={classes.CollapseListText} inset primary={children.TEXT} />
                                                    </ListItem>
                                            )
                                        }
                                    </List>
                                </Collapse>
                                </span>
                    ))}
                </List>
            </div>
        );




        return (
            <>
                <IconButton  className = {classes.menuIcon} onClick={OpenMobileMenu} color="inherit" aria-label="Menu">
                    <MenuIcon className={classes.menuIconColor}/>
                </IconButton>
                <Drawer open={Open} onClose={OpenMobileMenu}>
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={this.toggleDrawer('left', false)}
                        onKeyDown={this.toggleDrawer('left', false)}
                    >
                        {sideList}
                    </div>
                </Drawer>

            </>
        );
    }
}

TemporaryDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
};


export default connect(
    state => ({
        Open: state.OpenMobileMenu,
        Links: state.Navigation.data
    }),
    dispatch => ({
        OpenMobileMenu: () => dispatch({type: "OPEN_MOBILE_MENU"}),
        SelectNav: (data) => dispatch({type: "SELECT_NAV",payload: data})
    })
)(withStyles(styles)(TemporaryDrawer));