import React from 'react';
import {connect} from "react-redux";
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { useTheme } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles';
import logoImg from '../../img/logo_tio.png';

const useStyles = makeStyles(theme=>({
    logoLink:{
        background:`url(${logoImg}) no-repeat center`,
        backgroundSize:'contain',
        padding:'20% 0',
        width:100
    }

}))

const Logo = ({logo}) =>{
    const classes = useStyles();
    return  logo && logo.length > 0 ?
        <>
            <img src={logo.hasOwnProperty('href') && logo.href} alt={logo.hasOwnProperty('text') && logo.text}/>
        </>
        :
        <Link href={'/'}><div className = {classes.logoLink}></div></Link>
}



export default connect(
    state => ({
        logo: state.Logo.data
    }),
)(Logo);