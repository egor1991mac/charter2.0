import React, {Component} from 'react';
import {Input, Button} from 'antd';
import style from './style.module.css';
import {pt__3, mr__2, ml__2, my__2, px__1} from '../../style/indents.module.css';
import {flex} from '../../style/display.module.css';
import {align_items_center} from '../../style/flex.module.css';
import Remove from '@material-ui/icons/Remove';
import Add from '@material-ui/icons/Add';
class Index extends Component {
    state = {
        ...this.props.DATA
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.props.GET_DATA(this.state);
    }

    increment = name => value => {

        this.setState({[name]: this.state[name] >= 8 ? 8 : this.state[name] + 1});
    };
    dicrement = name => value => {
        if(name == 'adults'){
            this.setState({[name]: this.state[name] <= 1 ? 1 : this.state[name] - 1});
        }
        else {
            this.setState({[name]: this.state[name] < 1 ? 0 : this.state[name] - 1});
        }

    };
    handleChange = name => value => {
        this.setState({[name]: value});
    };

    render() {
        const {DATA, avia} = this.props;

        return (
            <div className={style.counter}>
                <div className={style.count}>
                    <label htmlFor="adults" className={`${style.label}`}>Взрослых: </label>
                    <div>
                        <Button type="primary" onClick={this.dicrement('adults')} className={`${mr__2} ${style.padding} ${style.dicrement}`}>-</Button>
                        <Input className={`${style.input}`} name='adults' min={0} max={10} defaultValue={DATA.adults}
                               value={this.state.adults} onChange={this.handleChange('adults')}/>
                        <Button type="primary" onClick={this.increment('adults')} className={`${ml__2} ${style.padding} ${style.increment}`}>+</Button>
                    </div>
                </div>

                <div className={`${style.count} ${my__2}`}>
                    <label htmlFor="children" className={`${style.label}`}>Детей: </label>
                    <div>
                        <Button type="primary" onClick={this.dicrement('children')} className={`${mr__2} ${style.padding} ${style.dicrement}`}> -</Button>
                        <Input className={`${style.input}`} name='children' min={0} max={10} defaultValue={DATA.children}
                               value={this.state.children} onChange={this.handleChange('children')}/>
                        <Button  type="primary" onClick={this.increment('children')} className={`${ml__2} ${style.padding} ${style.increment}`}> +</Button>
                    </div>
                </div>
                {
                    avia &&
                    <>
                        <div className={`${style.count}`}>
                            <label htmlFor="children" className={`${style.label}`}> Младенцев: </label>
                            <div>
                                <Button type="primary" onClick={this.dicrement('baby')} className={`${mr__2} ${style.padding} ${style.dicrement}`}> -</Button>
                                <Input className={`${style.input}`} name='baby' min={0} max={10}
                                       defaultValue={DATA.children} value={this.state.baby}
                                       onChange={this.handleChange('baby')}/>
                                <Button type="primary" onClick={this.increment('baby')} className={`${ml__2} ${style.padding} ${style.increment}`}> +</Button>
                            </div>
                        </div>
                    </>
                }
            </div>
        );
    }
}

export default Index;
