import React from 'react';
import Button from '@material-ui/core/Button';
import DropDown from '../dropdownMenu';
import {connect} from 'react-redux'

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme=>({
    flex:{
        display:'flex'

    },
    fonts:{
        //textTransform:'capitalize'
    }

}));

const Nav = ({links, HandleClick,children}) => {
    const classes = useStyles();
    if (links)
        return (
            <nav className={classes.flex}>
                {
                    links.map(item =>
                        !item.hasOwnProperty('children')
                            ?
                            <Button
                                href={item.hasOwnProperty('LINK') && item.LINK}
                                key={Math.random()}
                                className = {classes.fonts}
                                onClick={() => HandleClick(item.LINK)}>
                                {item.TEXT}
                            </Button>
                            : <DropDown
                                key={Math.random()}
                                parentItem={item}
                                childrenItem={item.children}
                                styles={classes.fonts}
                                onclick={HandleClick}/>
                    )

                }

            </nav>
        );
    else return null;
};


export default connect(
    state => ({
        links: state.Navigation.data
    }),
    dispatch => ({
        HandleClick: (data) => dispatch({type: "SELECT_NAV", payload: data})
    })
)(Nav)