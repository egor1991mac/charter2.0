import React from 'react';
import InputBase from '@material-ui/core/InputBase';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import { fade } from '@material-ui/core/styles/colorManipulator';

const styles = theme => ({

    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        border: '1px solid #b1b1b1',
        backgroundColor: fade(theme.palette.common.white, 0.2),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.9),
        },
        width: '100%',
        maxWidth:320,
        [theme.breakpoints.up('sm')]: {
            width: 'auto',
            marginLeft:'auto',
        },
        [theme.breakpoints.down('sm')]: {

            margin:'0 16px',
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(5),
        height: '100%',
        position: 'absolute',
        minWidth:32,
        //pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        right: 0,
        zIndex: 10,
        padding: 0,
        '&>span':{

        },
        '&>svg':{
            fill:'#b1b1b1',
        },

    },
    inputRoot: {
        color: '#212121',
        width: '100%',
        zIndex: 0
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing(2),
        transition: theme.transitions.create('width'),
        width: '100%',
        fontSize:14,
        zIndex: 0,
        [theme.breakpoints.up('sm')]: {
            width: 120,
            '&:focus': {
                width: 200,
            },
        },
        [theme.breakpoints.down('sm')]: {
            paddingLeft: '1rem'
        },
    },
});

const Search = ({classes}) => {
    return (
        <form className={classes.search} action="/poisk-po-saytu" method="get">

            <Button type={"submit"} className={classes.searchIcon}>
                <SearchIcon />
            </Button>
            <InputBase
                name="q"
                placeholder="Поиск…"
                classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                }}
            />
        </form>
    );
};

export default withStyles(styles)(Search);