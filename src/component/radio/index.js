import React, {Component} from 'react';
import { Radio } from 'antd';
import styles from './style.module.css';
import _ from 'lodash';
const RadioGroup = Radio.Group;




export default class radionField extends React.Component {
    state = {
        value: this.props.DATA[0]
    }

    onChange = (e) => {
        this.setState({
            value: e.target.value,
        });
        this.props.CHANGE(e);
    }

    render() {
        const {DATA,CHANGE,DEFAULT_DATA,styles} = this.props;
        return (
            <RadioGroup onChange={this.onChange} value={this.state.value} className={styles.elem} >
                {
                    DATA.map( item => <div key={item.key} >
                        <Radio value={item} className={styles.radio}> {item.name} </Radio></div> )
                }
            </RadioGroup>
        );
    }
}
