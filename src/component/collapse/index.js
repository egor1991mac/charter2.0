import React, {Component} from 'react';

class Index extends Component {
    constructor(props){
        super(props);
    }
    state = {
        open:false
    }
    render() {
        return (
            <div ref={this.collapse}>
                {this.props.children}
            </div>
        );
    }
}

export default Index;