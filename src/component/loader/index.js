import React from 'react';
import loader from './loader.module.css';
import style from './style.module.css';
import {mx__2} from '../../style/indents.module.css';
const Index = ({message}) => {
    return (
        <div className={style.loader}>
            <div className={style.body}>
                <div className={loader["lds-roller"]}>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
             <span className={mx__2}> {message ? message : 'Загрузка'}</span>
            </div>
        </div>
    );
};

export default Index;