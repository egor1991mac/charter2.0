import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import CssBaseline from '@material-ui/core/CssBaseline';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme=>({
    root: {
        background: 'rgb(249, 249, 249)',
        width: '100%',
        zIndex:1000,
    },


    toolbar: {
        maxWidth:'1200px',
        margin:'0 auto',
        width: '100%',

    },
    logo: {
        flexGrow: 1,
        [theme.breakpoints.down('lg')]: {
            padding:'0 1rem'
        }
    },
    search:{
        marginLeft:'auto'
    }

}));

function ElevationScroll(props) {
    const { children, window } = props;
    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 300,
        target: window ? window() : undefined,
    });

    return React.cloneElement(children, {
        elevation: trigger ? 4 : 0,
    });
}


export default function ButtonAppBar(props) {
    const classes = useStyles();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));
    const logo = props.children.find(item => item.key == 'logo');
    const nav = props.children.find(item => item.key == 'nav');
    const mobileNav = props.children.find(item => item.key == 'mobileNav');
    const search = props.children.find(item => item.key == 'search');
    return (
        <>
        <CssBaseline />
        <ElevationScroll {...props}>
            <div className={classes.root}>
                <AppBar  className={classes.root}>
                    <Toolbar className={classes.toolbar}>
                        <Grid container spacing={2} alignItems={"center"}>
                       <Grid item>
                           {logo}
                       </Grid>

                        {   matches ? (
                            <>
                            <Grid item> {nav} </Grid>
                                <Grid item xs container>
                                    {search}
                                </Grid>
                            </>
                            )
                            : <Grid item xs container>  {
                                mobileNav
                            }</Grid>
                        }
                        </Grid>
                    </Toolbar>
                </AppBar>
            </div>
        </ElevationScroll>
            <Toolbar></Toolbar>
            </>
    );
}


