import React, {Component, Suspense, lazy} from 'react';
import {Modal, Button} from 'antd';
import style from './style.module.css';
import 'antd/lib/modal/style/css';

const labelStyle = {
    minHeight: '14px'
};


class Popup extends Component {
    constructor(props) {
        super(props);
        this.Selected_Data = null;
    }
    state = {
        visible: false,
        DATA: this.props.DATA
    };
    componentWillReceiveProps(nextProps, nextContext) {
        //console.log(nextProps,'nextProps-popup');
        if(this.props != nextProps.DATA){
            this.setState(
                {
                    DATA:nextProps.DATA,
                    SELECTED_DATE_FROM: nextProps.SELECTED_DATE_FROM,
                    DEFAULT_DATA: nextProps.DEFAULT_DATA
                }
                );
        }
    }
    componentWillUnmount(){
        console.log('DESTROY');
    }


    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleTargetValue = (e) => {
        this.Selected_Data = e.target.value;
    };
    handleGetDataWithKey = (data) => {
        this.Selected_Data = data;
    };
    handleGetData = (data) =>{
        this.Selected_Data = data;
    };
    handleOk = () => {
        this.setState({
            visible: false,
        });
        //console.log(this.sele)
        this.props.GET_DATA && this.props.GET_DATA(this.Selected_Data);
    };

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    };

    render() {
        const {TEXT = '',
            DISABLE,
            DEFAULT_DATA,
            LABEL_TEXT,
            styles = '',
            icon = null,
            PLACEHOLDER = '',
            MODAL_TITLE_TEXT='',
            SELECTED_DATE_FROM
            } = this.props;

        const DATA = this.state.DATA == undefined ? this.props.DATA : this.state.DATA;
        
        return (
            <div className={styles.root}>
                <Button onClick={this.showModal} disabled={DISABLE} className={`${styles.elem}`}>
                    <div className={styles.tooltip}>
                        <div className={styles.icon}>
                            {icon}
                            {TEXT}
                        </div>
                        <div>{DEFAULT_DATA ? DEFAULT_DATA.airportCode : null}</div>
                    </div>
                    <div style={labelStyle} className={styles.elemText}>
                        {
                            LABEL_TEXT != null ? LABEL_TEXT : <div className={styles.placeHolder}>{PLACEHOLDER}</div>
                        }
                    </div>
                </Button>
                <Modal
                    title={MODAL_TITLE_TEXT}
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    okButtonProps={{
                        className: style.button
                    }}
                    cancelButtonProps = {{
                        ghost:true
                    }}
                    style={{
                        display:'flex',
                        width:'auto',
                        justifyContent:'center'
                    }}
                    bodyStyle={{
                        display:'flex',
                        justifyContent:'center',
                        alignItems:'center',
                        flexWrap:'wrap'
                    }}


                >
                    {
                        this.props.children &&
                            <div className={style.modal_body}>
                        <Suspense fallback={<div>Загрузка</div>}>
                            {
                                React.cloneElement(this.props.children, {
                                    styles,
                                    DATA,
                                    //DEFAULT_DATA,
                                    CHANGE: this.handleTargetValue,
                                    GET_DATA: this.handleGetDataWithKey,
                                    GET: this.handleGetData,
                                   //SELECTED_DATE_FROM
                                   ...this.state
                                })
                            }
                        </Suspense>
                            </div>
                    }
                </Modal>
            </div>
        );
    }
}

export default Popup;