import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    root: {
        display: 'flex',
    },
    paper: {
        marginRight: theme.spacing(2),
    },
    fonts:{
        textTransform:'capitalize',
        fontSize:'0.85rem'
    }
});

class MenuListComposition extends React.Component {
    state = {
        open: false,
    };

    handleToggle = () => {
        this.setState(state => ({ open: !state.open }));
    };

    handleClose = (event,link) =>  {
        if (this.anchorEl.contains(event.target)) {

            return;
        }
        this.props.onclick(link);
        this.setState({ open: false });

    };

    render() {
        const { classes,styles } = this.props;
        const { open } = this.state;

        return (
            <>
                    <Button
                        buttonRef={node => {
                            this.anchorEl = node;
                        }}
                        className = {classes.fonts}
                        aria-owns={open ? 'menu-list-grow' : undefined}
                        aria-haspopup="true"
                        onClick={this.handleToggle}
                    >
                        {this.props.parentItem.TEXT}
                    </Button>
                    <Popper open={open} anchorEl={this.anchorEl} transition disablePortal>
                        {({ TransitionProps, placement }) => (
                            <Grow
                                {...TransitionProps}
                                id="menu-list-grow"
                                style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                            >
                                <Paper>
                                    <ClickAwayListener onClickAway={this.handleClose}>
                                        <MenuList>
                                            {
                                                this.props.childrenItem.map(item=>
                                                    <MenuItem key={item.TEXT} onClick={(event)=>this.handleClose(event,item.LINK)} className={classes.fonts}>{item.TEXT}</MenuItem>
                                                )
                                            }
                                        </MenuList>
                                    </ClickAwayListener>
                                </Paper>
                            </Grow>
                        )}
                    </Popper>
             </>
        );
    }
}

MenuListComposition.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuListComposition);