import React from 'react';
import { Slider} from 'antd';
import style from './style.module.css';
import {my__4} from '../../style/indents.module.css';
export default class Slide extends React.Component {

    state= {
        night: this.props.DATA
    };

    handleChange = (data) => {
        this.setState({night:data});
        this.props.GET_DATA(data);
    };

    render() {
       const { DATA } = this.props;
       const { night } = this.state;

        return (
            <div className={my__4}>
                <label className={style.label}>
                    Ночей: c {night[0]} по {night[1]}
                </label>
                <Slider range defaultValue={DATA}
                        max={30}
                        min={4}
                        onChange={this.handleChange}
                        />
            </div>
        );
    }
}