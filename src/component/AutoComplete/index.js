import React from 'react';
import { AutoComplete,Input } from 'antd';
import './style.css';
import nanoid from 'nanoid';
const Option = AutoComplete.Option;
const OptGroup = AutoComplete.OptGroup;



export default class Complete extends React.Component {
    constructor(props){
        super(props);
        this.option = this.options(this.props);
    }
    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.DATA){
            this.option = this.options(nextProps);
        }
    };

    options = (props) =>  {
        let {DATA} = props;

        if(DATA !=undefined){
                return Object.keys(DATA).map((key,index) =>

                     <OptGroup key={`${key}_${nanoid(16)}`}  label={key}>
                    {
                        DATA[key].map((opt,index)=>
                                <Option key={`${opt.name}_${nanoid(16)}`} value={ opt.type != 'airport'
                                    ? opt.name : `(${opt.city_name}) - ${opt.name}`}>
                                    {
                                        opt.type == 'airport' ?  <span>{opt.city_name}-{opt.name}({opt.code})</span>
                                            :<span>{opt.name}({opt.code})</span>
                                    }
                                </Option>
                        )
                    }
                </OptGroup>)
            }
        };

    handleSearch = (value) => {
        this.props.GET_DATA(value);
    };
    onSelect = (value,option) => {

        if(value.indexOf('-') != -1){
            value.split(' - ');
            this.props.SET_DATA(value.split(' - ')[1]);
        }
        else{
            this.props.SET_DATA(value);
        }

    }

    render() {

        const {styles, icon, TEXT, PLACEHOLDER,DATA,DEFAULT_DATA} = this.props;



        return (
            <div  className={`${styles.root}`}>
                <div className={`${styles.elem}`}>
                    <div className={styles.tooltip}>
                        <div className={styles.icon}>
                            {icon}
                            {TEXT}
                        </div>
                        <div>{DEFAULT_DATA}</div>
                    </div>

                        <AutoComplete
                            dataSource={this.option ? this.option : []}
                            onSelect={this.onSelect}
                            onSearch={this.handleSearch}
                            placeholder={PLACEHOLDER}
                            optionLabelProp="value"
                        />
                </div>
            </div>
        );
    }
}
