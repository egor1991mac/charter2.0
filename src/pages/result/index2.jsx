import React, {Suspense, lazy, Component} from 'react';
import FlightLand from '@material-ui/icons/FlightLand';
import FlightTakeoff from '@material-ui/icons/FlightTakeoff';
import style from './style.module.css';
import {all__24, sm__12, lg__5, lg__10,lg__4, row} from '../../style/grid.module.css';
import {mb__2, px__1} from '../../style/indents.module.css';
import moment from 'moment';



export default class Index extends Component {


    state = {
        data: this.props.data
    }

 
    render() {

        let {data,children,count} = this.props;
        //const {data} = this.state;

        if (data) return (
            <>
                {
                    data.map((item, index) =>
                        <div className={`${style.charterItem} ${row} ${mb__2}`} key={`${item.charterKey}${index}`} >
                            <div className={`${style.host} ${style.from} ${all__24} ${sm__12} ${lg__4}`}>


                                <img src={item.host.img} alt="" style={{width:'100px'}}/>
                            </div>
                            <div className={`${style.from} ${all__24} ${sm__12} ${lg__5}`}>
                                <header className={style.header}>
                                    <FlightTakeoff/>
                                    <span className={`${px__1}`}>
                                        {item.cityFromName}
                                    </span>
                                    <span>
                                        {moment(item.dateTo).format('DD.MM.YYYY')}
                                </span>

                                </header>
                                <ul className={style.ul}>
                                    <li className={style.li}>
                                        {item.cityFromName}({item.airportFromCode})
                                        - {item.cityToName}({item.airportToCode})
                                    </li>
                                    <li>
                                        {item.charterTime}
                                    </li>
                                    <li>
                                        {item.airlineBackName}
                                    </li>
                                </ul>
                            </div>
                            <div className={`${style.to} ${all__24} ${sm__12} ${lg__5}`}>
                                <header className={style.header}>
                                    <FlightLand/>
                                    <span className={`${px__1}`}>
                                        {item.cityToName}
                                </span>
                                    <span >
                                        {moment(item.dateBack).format('DD.MM.YYYY')}
                                    </span>

                                </header>
                                <ul className={style.ul}>
                                    <li>
                                        {item.cityToName}({item.airportToCode})
                                        - {item.cityFromName}({item.airportFromCode})
                                    </li>
                                    <li>
                                        {item.charterBackTime}
                                    </li>
                                    <li>
                                        {item.airlineBackName}
                                    </li>
                                </ul>
                            </div>
                            <div className={`${style.info} ${all__24} ${lg__10}`}>
                                <ul className={style.ul}>
                                    <li>
                                        {count[0]} <span>{count[0] > 1 ? 'Взрослых' : 'Взрослый'}</span>, {count[1]} <span>{count[1]>1 ? 'детей' : 'ребенок'}</span>
                                    </li>
                                    <li>Проверенный и ручной багаж, включенный в стоимость билета</li>
                                    <li>Трансфер в оба конца + трансфер из аэропорта</li>
                                </ul>
                                <footer className={style.footer}>
                                    <span className={style.price}>{Math.round(item.cost*count[0]+item.cost*count[1])} <b>BYN</b></span>
                                    <Suspense fallback={<div>Загрузка</div>}>
                                        {
                                            children
                                        }
                                    </Suspense>
                                </footer>
                            </div>
                        </div>
                    )

                }

            </>
        )
    }
}




