import React, {Component} from 'react';
import {all__24, sm__12, lg__2, lg__3, lg__4, lg__5, lg__6, lg__8, lg__7, row} from '../../../style/grid.module.css';
import style from './style.module.css';
import {MESSAGE_CHARTER} from '../../../config';
import nanoid from 'nanoid';

class detailResult extends Component {
    state = {
        ...this.props,
    };
    componentWillReceiveProps(nextProps, nextContext) {
        if(this.props != nextProps){
            this.setState({...nextProps});

        }
    }
    
    render() {
        const {DATA, children, count,way} = this.state;
        
        return (

            way ?
                <>
                    {
                        DATA[Object.keys(DATA)].map((item, index) =>
                            <div
                                className={`${row} ${style.detail} ${index == DATA[Object.keys(DATA)].length - 1 && style.borderBottomNone}`}
                                key={nanoid()}>
                                <div className={`${all__24} ${lg__3} $ ${style.logo}`}>
                                    <header className={style.header}>
                                        {MESSAGE_CHARTER.result.host}
                                    </header>
                                    {
                                        item.host.hasOwnProperty('logo') &&
                                        <div className={style.img} style={{background:`url(${item.host.logo})`,backgroundSize:'contain',bacgroundRepeat:'no-repeat'}} alt={item.host.name}></div>
                                    }

                                </div>
                                <div className={`${all__24} ${lg__2} ${style.to}`}>
                                    <header className={style.header}>
                                        {MESSAGE_CHARTER.result.class}
                                    </header>
                                    <div className={style.content}>
                                        Эконом
                                    </div>
                                </div>
                                <div className={`${all__24} ${lg__3} ${style.from}`}>
                                    <header className={style.header}>
                                        {MESSAGE_CHARTER.result.way_from}
                                    </header>
                                    <div className={style.content}>

                                        № {item.charterNumber}

                                    </div>
                                </div>
                                <div className={`${all__24} ${lg__5} ${style.from}`}>
                                    <header className={style.header}>
                                        {MESSAGE_CHARTER.result.time_from}
                                    </header>

                                    <ul className={style.list}>
                                        <li><b>вылет:</b> {item.charterTime}</li>
                                        <li><b>прилет:</b> {item.charterTimeArr}</li>
                                    </ul>

                                </div>
                                <div className={`${all__24} ${lg__3} ${style.from}`}>
                                    <header className={style.header}>
                                        {MESSAGE_CHARTER.result.way_back}
                                    </header>
                                    <div className={style.content}>
                                        № {item.charterBackNumber}
                                    </div>
                                </div>
                                <div className={`${all__24} ${lg__5} ${style.to}`}>
                                    <header className={style.header}>
                                        {MESSAGE_CHARTER.result.time_back}
                                    </header>

                                    <ul className={style.list}>
                                        <li><b>вылет:</b> {item.charterBackTime}</li>
                                        <li><b>прилет:</b> {item.charterBackTimeArr}</li>
                                    </ul>
                                </div>


                                <div className={`${all__24} ${lg__3} ${style.price}`}>
                            <span>{Math.ceil((item.cost * count[0] + item.cost * count[1]) * 100) / 100}
                                <small> BYN </small></span>
                                    {
                                        React.cloneElement(
                                            children,
                                            {
                                                CHARTER:
                                                    {
                                                        CHARTER: item,
                                                        TOTALCOST: Math.ceil((item.cost * count[0] + item.cost * count[1]) * 100) / 100,
                                                        PEOPLE: {
                                                            adults: count[0],
                                                            children: count[1]
                                                        }
                                                    }
                                            })
                                    }
                                </div>
                            </div>
                        )
                    }
                </>
                :
                <>
                    {
                        DATA[Object.keys(DATA)].map((item, index) =>
                            <div
                                className={`${row} ${style.detail} ${index == DATA[Object.keys(DATA)].length - 1 && style.borderBottomNone}`}
                                key={nanoid()}>
                                <div className={`${all__24} ${lg__5} $ ${style.logo}`}>
                                    <header className={style.header}>
                                        {MESSAGE_CHARTER.result.host}
                                    </header>

                                    {
                                        item.host.hasOwnProperty('logo') &&
                                        <div className={style.img} style={{background:`url(${item.host.logo})`,backgroundSize:'contain',bacgroundRepeat:'no-repeat'}} alt={item.host.name}></div>
                                    }


                                </div>
                                <div className={`${all__24} ${lg__5} ${style.to}`}>
                                    <header className={style.header}>
                                        {MESSAGE_CHARTER.result.class}
                                    </header>
                                    <div className={style.content}>
                                        Эконом
                                    </div>
                                </div>
                                <div className={`${all__24} ${lg__5} ${style.from}`}>
                                    <header className={style.header}>
                                        {MESSAGE_CHARTER.result.way_from}
                                    </header>
                                    <div className={style.content}>

                                        № {item.charterNumber}

                                    </div>
                                </div>
                                <div className={`${all__24} ${lg__5} ${style.from}`}>
                                    <header className={style.header}>
                                        {MESSAGE_CHARTER.result.time_from}
                                    </header>

                                    <ul className={style.list}>
                                        <li><b>вылет:</b> {item.charterTime}</li>
                                        <li><b>прилет:</b> {item.charterTimeArr}</li>
                                    </ul>

                                </div>
                                <div className={`${all__24} ${lg__4} ${style.price}`}>
                            <span>{Math.ceil((item.cost * count[0] + item.cost * count[1]) * 100) / 100}
                                <small> BYN </small></span>
                                    {
                                        React.cloneElement(
                                            children,
                                            {
                                                CHARTER:
                                                    {
                                                        CHARTER: item,
                                                        TOTALCOST: Math.ceil((item.cost * count[0] + item.cost * count[1]) * 100) / 100,
                                                        PEOPLE: {
                                                            adults: count[0],
                                                            children: count[1]
                                                        }
                                                    }
                                            })
                                    }
                                </div>
                            </div>
                        )
                    }
                </>
        );
    }
}



export default detailResult;