import React, {Suspense, lazy, PureComponent} from 'react';
import FlightLand from '@material-ui/icons/FlightLand';
import FlightTakeoff from '@material-ui/icons/FlightTakeoff';
import style from './style.module.css';
import {all__24, sm__12, lg__5, lg__10, lg__7, lg__12, row} from '../../style/grid.module.css';
import {mb__2, mt__3, px__1, pl__1, mb_sm__0, mb__1,mb_lg__0 } from '../../style/indents.module.css';

import DetailResult from './detailResult'
import moment from 'moment';
import nanoid from 'nanoid';
import {Button} from 'antd';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';


const styles = ({
    root: {
        margin: '0 -1rem',
    },

});



class Index extends PureComponent {

    constructor(props) {
        super(props);
        this.result = React.createRef();
        this.collapseRef = this.props.data.map(item => React.createRef());
        this.ButtonRef = this.props.data.map(item => React.createRef());

    }

    state = {
       ...this.props
    };

    componentWillReceiveProps(nextProps, nextContext) {
        if (this.props != nextProps) {
            this.setState({...nextProps});
            this.collapseRef = nextProps.data.map(item => React.createRef());
            this.ButtonRef = nextProps.data.map(item => React.createRef());
            const {data} = nextProps;
            data.forEach(elem => {
                elem[Object.keys(elem)].forEach(charter => {
                    charter.price = [];
                    charter.price.push(charter.cost);
                });
                elem[Object.keys(elem)].forEach(charter => {
                    charter.price.sort();
                })
            });
        }
    }

    componentWillMount() {
        const {data} = this.props;

        data.forEach(elem => {
            elem[Object.keys(elem)].forEach(charter => {
                charter.price = [];
                charter.price.push(charter.cost);
            });
            elem[Object.keys(elem)].forEach(charter => {
                charter.price.sort();
            })
        });
    }

    handleCollapse = (index) => (e) => {
        this.collapseRef[index].current.classList.toggle(style.activeCollapse);
        this.ButtonRef[index].current.buttonNode.classList.toggle(style.active);

        if (this.ButtonRef[index].current.buttonNode.classList.contains(style.active)) {
            this.ButtonRef[index].current.buttonNode.innerText = 'Скрыть предложения';
            this.ButtonRef[index].current.buttonNode.closest(`.${style.charterItem}`).classList.add(`${style.active}`);//.classList.remove(`${style.active}`);
        } else {
            this.ButtonRef[index].current.buttonNode.innerText = 'Отобразить предложения';
            this.ButtonRef[index].current.buttonNode.closest(`.${style.charterItem}`).classList.remove(`${style.active}`);//.classList.add(`${style.active}`);
        }
    };


    render() {
        const {data, count, way, classes} = this.state;

        if (data) return (
            way ?
                <>
                    {
                        data.map((elem, indexElem) =>
                            elem[Object.keys(elem)].map((charter, index) =>

                                index == 0 &&
                                <Paper className={classes.root} key={`chareter_item_list-${index}`}>
                                    <div id={`charter_item_list-${index}`} className={`${style.charterItem} ${row}`} style={{margin:'1rem 0'}}
                                         key={nanoid()}>
                                        <div className={`${style.from} ${all__24} ${sm__12} ${lg__7}`}>
                                            <header className={style.header}>
                                                <FlightTakeoff/>
                                                <span className={`${px__1}`}>
                                            {charter.cityFromName}
                                        </span>
                                                <span>
                                            {moment(charter.dateTo).format('DD.MM.YYYY')}
                                        </span>

                                            </header>
                                            <ul className={style.ul}>
                                                <li className={style.li}>
                                                    {charter.cityFromName}({charter.airportFromCode})
                                                    - {charter.cityToName}({charter.airportToCode})
                                                </li>
                                                {charter.airlineName &&
                                                <li>
                                                    {charter.airlineName}
                                                </li>
                                                }

                                            </ul>
                                        </div>
                                        <div className={`${style.to} ${all__24} ${sm__12} ${lg__7}`}>
                                            <header className={style.header}>
                                                <FlightLand/>
                                                    <span className={`${px__1}`}>
                                                    {charter.cityToName}

                                            </span>
                                                <span>
                                                    {moment(charter.dateBack).format('DD.MM.YYYY')}
                                                </span>

                                            </header>
                                            <ul className={style.ul}>
                                                <li>
                                                    {charter.cityToName}({charter.airportToCode})
                                                    - {charter.cityFromName}({charter.airportFromCode})
                                                </li>
                                                {
                                                    charter.airlineBackName &&
                                                    <li>
                                                        {charter.airlineBackName}
                                                    </li>
                                                }

                                            </ul>
                                        </div>
                                        <div className={`${style.info} ${all__24} ${lg__10}`}>
                                            <ul className={style.ul}>
                                                <li>
                                                    {count && count[0]}
                                                    <span style={{marginRight:'5px'}}>{count[0] > 1 ? ' Взрослых' : ' Взрослый'},</span>
                                                    {count[1]}
                                                    <span style={{marginLeft:'5px'}}>
                                                    {
                                                        (()=>{
                                                            let data = 'Детей';
                                                            switch (count[1]) {
                                                                case 0:
                                                                    data = 'детей';
                                                                    break;
                                                                case 1:
                                                                    data = 'ребенок';
                                                                    break;
                                                                case count[0]>1:
                                                                    data = 'детей';
                                                            }
                                                            return data;
                                                        })()
                                                    }
                                                    </span>
                                                </li>

                                                <li>{window.FormMessage.MESSAGE_CHARTER.result.info_text}</li>

                                            </ul>
                                            <footer className={style.footer}>
                                                <span className={`${style.price}  ${mb__1} ${mb_sm__0}`}>
                                            {

                                                charter.price[0] == charter.price[charter.price.length - 1]
                                                    ? `от ${Math.ceil((charter.price[0] * count[0] + charter.price[0] * count[1]) * 100) / 100}`
                                                    : `от ${Math.ceil((charter.price[0] * count[0] + charter.price[0] * count[1]) * 100) / 100} - ${Math.ceil((charter.price[charter.price.length - 1] * count[0] + charter.price[charter.price.length - 1] * count[1]) * 100) / 100}`

                                            }

                                            <small> BYN </small>
                                        </span>
                                                <Button ref={this.ButtonRef[indexElem]} className={`${style.Open}`}
                                                        onClick={this.handleCollapse(indexElem)}>
                                                    <span>Отобразить предложения</span>
                                                </Button>
                                            </footer>
                                        </div>
                                        <div className={style.collapse} data-atr={indexElem}>
                                            <div className={`${all__24} ${style.collapseContent}`}
                                                 ref={this.collapseRef[indexElem]}>
                                                <DetailResult DATA={elem} count={count} way={way}>
                                                    {this.props.children}
                                                </DetailResult>

                                            </div>
                                        </div>
                                    </div>


                        </Paper>
                            )
                        )
                    }
                </>
                :
                <>
                    {

                        data.map((elem, indexElem) =>
                            elem[Object.keys(elem)].map((charter, index) =>
                                index == 0 &&
                                <Paper className={classes.root} key={`chareter_item_list-${index}`}>
                                    <div id={`charter_item_list-${index}`} className={`${style.charterItem} ${row}`} style={{margin:'1rem 0'}}>
                                        <div className={`${style.from} ${all__24} ${sm__12} ${lg__12}`}>
                                            <header className={style.header}>

                                                <FlightTakeoff/>
                                                <span className={`${px__1}`}>
                                                    {charter.cityFromName}
                                                    </span>
                                                    <span>
                                                            {moment(charter.dateTo).format('DD.MM.YYYY')}
                                                    </span>


                                            </header>
                                            <ul className={style.ul}>
                                                <li className={style.li}>
                                                    {charter.cityFromName}({charter.airportFromCode})
                                                    - {charter.cityToName}({charter.airportToCode})
                                                </li>
                                                {
                                                    charter.airlineName &&
                                                    <li>
                                                        {charter.airlineName}
                                                    </li>
                                                }
                                            </ul>
                                        </div>
                                        <div className={`${style.info} ${all__24} ${sm__12} ${lg__12}`}>
                                            <ul className={style.ul}>
                                                <li>
                                                    {count && count[0]}
                                                    <span style={{marginRight:'5px'}}>{count[0] > 1 ? ' Взрослых' : ' Взрослый'},</span>
                                                    {count[1]}
                                                    <span style={{marginLeft:'5px'}}>
                                                    {
                                                        (()=>{
                                                            let data = 'Детей';
                                                            switch (count[1]) {
                                                                case 0:
                                                                    data = 'детей';
                                                                    break;
                                                                case 1:
                                                                    data = 'ребенок';
                                                                    break;
                                                                case count[0]>1:
                                                                    data = 'детей';
                                                            }
                                                            return data;
                                                        })()
                                                    }
                                                    </span>
                                                </li>
                                                <li>{window.FormMessage.MESSAGE_CHARTER.result.info_text}</li>

                                            </ul>
                                            <footer className={style.footer}>
                                                <span className={`${style.price}  ${mb__1} ${mb_sm__0}`}>
                                            {

                                                charter.price[0] == charter.price[charter.price.length - 1]
                                                    ? `от ${Math.ceil((charter.price[0] * count[0] + charter.price[0] * count[1]) * 100) / 100}`
                                                    : `от ${Math.ceil((charter.price[0] * count[0] + charter.price[0] * count[1]) * 100) / 100} - ${Math.ceil((charter.price[charter.price.length - 1] * count[0] + charter.price[charter.price.length - 1] * count[1]) * 100) / 100}`

                                            }

                                                    <small> BYN </small>
                                        </span>
                                                <Button ref={this.ButtonRef[indexElem]} className={`${style.Open}`}
                                                        onClick={this.handleCollapse(indexElem)}>
                                                    <span>Отобразить предложения</span>
                                                </Button>
                                            </footer>
                                        </div>
                                        <div className={style.collapse} data-atr={indexElem}>
                                            <div className={`${all__24} ${style.collapseContent}`}
                                                 ref={this.collapseRef[indexElem]}>
                                                <DetailResult DATA={elem} count={count} way={way}>
                                                    {this.props.children}
                                                </DetailResult>
                                            </div>
                                        </div>
                                    </div>
                                </Paper>
                            ))
                    }
                </>
        )
    }
}

export  default withStyles(styles)(Index)


