import React, {Component} from 'react';
import WithAvia from '../../hoc/withAvia';
import {Button, Spin} from "antd";
import {isEmpty} from "lodash";
import moment from 'moment';
import styleForm from './style.module.css';
import Place from '@material-ui/icons/Place';
import Calendar from '@material-ui/icons/CalendarToday';
import Star from '@material-ui/icons/Star';
import People from '@material-ui/icons/People';

//style
import {wrap, row, all__24, lg__2,lg__4,lg__5,lg__3, sm__12, lg__auto} from '../../style/grid.module.css';
import  {py__2, px__2, px__4} from '../../style/indents.module.css';
import {none, flex_important} from '../../style/display.module.css';
import {flex_grow} from '../../style/flex.module.css';


//componets
import AutoComplete from '../../component/AutoComplete';
import RadioField from '../../component/radio';
import Popup from '../../component/popup';
import DayPicker from '../../component/daypicker';
import Counter from '../../component/counter';
import {AIR_CLASS} from "../../config";
import { Radio } from 'antd';

const RadioGroup = Radio.Group;


export default class AviaForm extends Component {
    state = {
        value: false,
    };

    onChange = (callback) =>(e) => {

        this.setState({
            value: e.target.value,
        });
        callback(e.target.value);
    };
    render() {
        return (
                <WithAvia>
                    {
                       data =>
                           <div className={`${wrap} ${styleForm.findForm} `}  >
                            <div className={`${row} ${px__4} ${py__2}`}>
                                <div className={all__24}>
                                    <RadioGroup onChange={this.onChange(data.GET_DATE('WAY'))} value={this.state.value} className={styleForm.radio}>
                                        <Radio value={false} className={`${styleForm.fontWhite}`}>В одну сторону</Radio>
                                        <Radio value={true} className={`${styleForm.fontWhite}`}>В обе стороны</Radio>
                                    </RadioGroup>
                                </div>
                                <AutoComplete
                                    styles = {{
                                        root:[all__24,sm__12,lg__4].join(' '),
                                        elem:[styleForm.autoComplete,px__2,].join(' '),
                                        tooltip:styleForm.tittle,
                                        icon:styleForm.icon,
                                        placeHolder: styleForm.placeHolder
                                    }}
                                    DEFAULT_DATA = {data.from ? data.from.code : null}
                                    TEXT = {data.MESSAGE_AVIA.from.tittle}
                                    icon = {<Place/>}
                                    GET_DATA={data.GET_DATA}
                                    SET_DATA={data.SET_DATA_AIR('from')}
                                    PLACEHOLDER ={data.MESSAGE_AVIA.from.placeholder}
                                    DATA = {data.data}
                                />
                                <AutoComplete
                                    styles = {{
                                        root:[all__24,sm__12,lg__4].join(' '),
                                        elem:[styleForm.autoComplete,px__2,].join(' '),
                                        tooltip:styleForm.tittle,
                                        icon:styleForm.icon,
                                        placeHolder: styleForm.placeHolder
                                    }}
                                    GET_DATA={data.GET_DATA}
                                    TEXT ={data.MESSAGE_AVIA.to.tittle}
                                    icon = {<Place/>}
                                    PLACEHOLDER ={data.MESSAGE_AVIA.to.placeholder}
                                    SET_DATA={data.SET_DATA_AIR('to')}
                                    DATA = {data.data}
                                    DEFAULT_DATA = {data.to ? data.to.code : null}
                                />
                                <Popup
                                    LABEL_TEXT = {moment(new Date(data.DATE_FROM)).format('DD.MM.YYYY')}
                                    DEFAULT_DATA = {data.DATE_FROM == undefined ? null : data.DATE_FROM}
                                    styles = {{
                                        root:[all__24,sm__12,lg__3].join(' '),
                                        elem:styleForm.formItem,
                                        tooltip:styleForm.tittle,
                                        icon:styleForm.icon,
                                        placeHolder: styleForm.placeHolder
                                    }}
                                    icon = {<Calendar/>}
                                    TEXT = {data.MESSAGE_AVIA.date_from.tittle}
                                    MODAL_TITLE_TEXT = {<span className={styleForm.ModalTitle}>{data.MESSAGE_AVIA.date_from.popup}</span>}
                                    buttonPopupStyle = {styleForm.formItem}
                                    GET_DATA = {data.GET_DATE('DATE_FROM')}
                                >
                                    <DayPicker
                                        def={true} MINDATE={new Date()}/>
                                </Popup>
                                {
                                    this.state.value &&
                                    <Popup
                                        LABEL_TEXT = { data.DATE_TO == undefined
                                            ? moment(data.DATE_FROM).add(1,'days').format('DD.MM.YYYY')
                                            : moment(new Date(data.DATE_TO)).format('DD.MM.YYYY')
                                        }
                                        DEFAULT_DATA = {data.DATE_TO == undefined ? null : data.DATE_TO}
                                        styles = {{
                                            root:[all__24,sm__12,lg__3].join(' '),
                                            elem:styleForm.formItem,
                                            tooltip:styleForm.tittle,
                                            icon:styleForm.icon,
                                            placeHolder: styleForm.placeHolder
                                        }}
                                        icon = {<Calendar/>}
                                        TEXT = {data.MESSAGE_AVIA.date_to.tittle}
                                        MODAL_TITLE_TEXT = {<span className={styleForm.ModalTitle}>{data.MESSAGE_AVIA.date_from.popup}</span>}
                                        buttonPopupStyle = {styleForm.formItem}
                                        GET_DATA = {data.GET_DATE('DATE_TO')}
                                    >
                                        <DayPicker def={true} MINDATE={data.DATE_FROM ? data.DATE_FROM : new Date()}/>
                                    </Popup>
                                }

                                <Popup
                                    LABEL_TEXT={
                                        Object.keys(data.PEOPLE).length == 0
                                            ? `${data.DEFAULT_PEOPLE.adults} взр, ${data.DEFAULT_PEOPLE.children} дет`
                                            : `${data.PEOPLE.adults} взр, ${data.PEOPLE.children} дет, ${data.PEOPLE.baby} млд`
                                    }
                                    DATA={data.PEOPLE}
                                    styles = {{
                                        root:[all__24,sm__12,lg__4].join(' '),
                                        elem:styleForm.formItem,
                                        tooltip:styleForm.tittle,
                                        icon:styleForm.icon
                                    }}

                                    GET_DATA = {data.GET_DATE('PEOPLE',undefined)}
                                    icon = {<People/>}
                                    TEXT = {data.MESSAGE_AVIA.people.tittle}
                                    MODAL_TITLE_TEXT = {<span className={styleForm.ModalTitle}>{data.MESSAGE_AVIA.people.popup}</span>}
                                >
                                    <Counter  avia = {true}/>
                                </Popup>
                                <Popup
                                    LABEL_TEXT={ Array.isArray(data.AIR_CLASS) ? data.AIR_CLASS[0].name : data.AIR_CLASS.name }
                                    DEFAULT_DATA = {data.AIR_CLASS.length > 1 ? data.AIR_CLASS[0] : data.AIR_CLASS}
                                    styles = {{
                                        root:[all__24,sm__12,lg__3].join(' '),
                                        radio: [styleForm.radio,styleForm.fontColor].join(' '),
                                        elem:styleForm.formItem,
                                        tooltip:styleForm.tittle,
                                        icon:styleForm.icon
                                    }}
                                    DATA={AIR_CLASS}
                                    GET_DATA = {data.GET_DATE('AIR_CLASS',undefined)}
                                    icon = {<Star/>}
                                    TEXT = {data.MESSAGE_AVIA.class.tittle}
                                    MODAL_TITLE_TEXT = {<span className={styleForm.ModalTitle}>{data.MESSAGE_AVIA.class.popup}</span>}
                                >
                                    <RadioField/>
                                </Popup>
                                <div className={`${lg__auto} ${flex_grow}`}>
                                    <Button icon="search"
                                            className={styleForm.buttonFind}
                                            onClick={data.SEARCH_AVIA}
                                            disabled={(data.from == null || data.to == null) ? true : false}
                                    >{data.MESSAGE_AVIA.submit.tittle}
                                    </Button>
                                </div>
                            </div>
                        </div>
                    }
                </WithAvia>


        );
    }
}

