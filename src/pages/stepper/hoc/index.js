import React, {Component} from 'react';
import {isEqual} from 'lodash';
import axios from 'axios';
import * as qs from "qs";

function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time))
}



class withStepper extends Component {
    componentDidUpdate(prevProps, prevState, snapshot) {

    }

    state={
        loading:false,
        alert:[],
        diseases:window.FormSanatoriums,

    };


    sendData = async () =>{
        const {loading, alert, diseases,selected_kinds,selected_diseases,date,days,people,contact_form,contact_text} = this.state;
        let body = {
            diseases: selected_diseases.value.Name,
            kinds:selected_kinds.value.Name,
            date:date.data,
            days:days.data,
            people:people.data,
            fio:contact_form.data.name.value,
            email:contact_form.data.email.value,
            phone: contact_form.data.phone.value,
            text:contact_text
        };

        body = qs.stringify(body,{arrayFormat :  ' brackets '});

        try {

            this.setState({loading:true});
            await sleep(1500);
            const result = await axios.post('https://www.tio.by/ajax/search_tour.php',body);
            this.setState({loading:false,
                alert:{type:'success',text:"Спасибо за бронирование. Мы с вами свяжемся"}
            });
        }
        catch (e) {

            this.setState({loading:false,
                alert:{type:'error',text:"Извините. Произошла ошибка."}
            });
        }
    };
    filtred_value = (name,value) => this.state[name].find(item=> item.Id === parseInt(value));


    select_value = name => value =>{
        const {selected_diseases} = this.state;
        let data = [];
        if(selected_diseases){
            console.log(value);
            data = selected_diseases.value.kinds.find(item=>item.Id === parseInt(value.data))
        }
        else{
            data = this.filtred_value(name,value.data);
        }

        this.setState({
            [`selected_${name}`]:{
                valid: value.valid,
                value: data}
        });
    };
    select_data = name => value =>{
        this.setState({[name]:value});
    };

    render() {
        return this.props.children(
            {
                ...this.state,
                select_value: this.select_value,
                select_data:this.select_data,
                send_data: this.sendData
            })

    }
}

export default withStepper;
