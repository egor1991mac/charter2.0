import React, {useEffect,useState} from 'react';
import Popup from '../../component/popup';
import {all__24, all__auto, lg__3, lg__4, lg__5, lg__6, row, sm__12, wrap} from "../../style/grid.module.css";
import styleForm from "./style.module.css";
import {px__4, py__2} from "../../style/indents.module.css";
import Place from '@material-ui/icons/Place';
import Calendar from '@material-ui/icons/CalendarToday';
import moment from "../CharterForm";
import {_FORMAT_DATE} from "../../api";
import Counter from '../../component/counter';
import People from '@material-ui/icons/People';
import {flex_grow} from "../../style/flex.module.css";
import {Button} from "antd";

import {connect} from 'react-redux'


const Busform = ({links}) => {
    const {MESSAGE_BUS} = window.FormMessage;

    return (

            <div className={`${wrap} ${styleForm.findForm}`}  >

                <div className={`${row} ${px__4} ${py__2} ${styleForm.align_items}`} >
                    <Popup
                        styles = {{
                            root:[all__24,sm__12,lg__5].join(' '),
                            elem:styleForm.formItem,
                            tooltip:styleForm.tittle,
                            icon:styleForm.icon,
                            placeHolder: styleForm.placeHolder
                        }}
                        icon = {<Place/>}
                        TEXT = {MESSAGE_BUS.from.tittle}
                    >
                    </Popup>
                    <Popup
                        styles = {{
                            root:[all__24,sm__12,lg__5].join(' '),
                            elem:styleForm.formItem,
                            tooltip:styleForm.tittle,
                            icon:styleForm.icon,
                            placeHolder: styleForm.placeHolder
                        }}
                        icon = {<Place/>}
                        TEXT = {MESSAGE_BUS.to.tittle}
                    >
                    </Popup>
                    <Popup
                           styles = {{
                               root:[all__24,sm__12,lg__5].join(' '),
                               elem:styleForm.formItem,
                               tooltip:styleForm.tittle,
                               icon:styleForm.icon,
                               placeHolder: styleForm.placeHolder
                           }}
                           icon = {<Calendar/>}
                           TEXT = {MESSAGE_BUS.date.tittle}
                          >
                    </Popup>
                    <Popup

                        DATA={{
                            adults: 1,
                            children: 0,
                            baby: 0
                        }}
                        styles = {{
                            root:[all__24,sm__12,lg__4].join(' '),
                            elem:styleForm.formItem,
                            tooltip:styleForm.tittle,
                            icon:styleForm.icon
                        }}


                        icon = {<People/>}
                        TEXT = {MESSAGE_BUS.people.tittle}
                        MODAL_TITLE_TEXT = {<span className={styleForm.ModalTitle}>{MESSAGE_BUS.people.popup}</span>}

                    >
                        <Counter  avia = {false}/>
                    </Popup>
                    <div className={[all__auto,flex_grow].join(' ')}>
                        <Button icon="search"
                                className={styleForm.buttonFind}
                        >{
                            MESSAGE_BUS.submit.tittle
                        }
                        </Button>
                    </div>
                </div>

            </div>
    );
};


export default connect(
    state => ({
        links: state.Navigation.data
    }),
    dispatch => ({
        HandleClick: (data) => dispatch({type: "SELECT_NAV", payload: data})
    })
)(Busform)