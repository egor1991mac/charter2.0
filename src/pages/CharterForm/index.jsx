import React, {Component} from 'react';
import WithCharter from '../../hoc/withCharter';

import Loader from '../../component/loader';
import {Button, Spin} from "antd";
import {isEmpty} from "lodash";
import moment from 'moment';
import styleForm from './style.module.css';
import Place from '@material-ui/icons/Place';
import Calendar from '@material-ui/icons/CalendarToday';
import AccessTime from '@material-ui/icons/AccessTime';
import People from '@material-ui/icons/People';
import {_FORMAT_DATE} from "../../api";
import { Radio } from 'antd';
import axios from 'axios';
//style
import {wrap, row, lg__4, all__24, lg__5,lg__3, sm__12,all__auto} from '../../style/grid.module.css';
import  {py__2, px__4,mr__4, mb__1} from '../../style/indents.module.css';
import  {flex_grow} from '../../style/flex.module.css';
import {none} from '../../style/display.module.css';

//componets
import Popup from '../../component/popup';
import DayPicker from '../../component/daypicker';
import Slider from '../../component/slider';
import Counter from '../../component/counter';
import Tree from '../../component/tree';
import Tree2 from '../../component/tree2';

const RadioGroup = Radio.Group;

export default class CharterForm extends Component {
    constructor(props){
        super(props);
        this.Data = undefined;
    }
    state={
        data: undefined
    };
    async componentDidMount() {
        try{
            axios.post(window.CharterDataPost)
                .then(result=>{
                    if(result){

                        this.setState((state)=>({data: result.data}));
                    }
                });

        }
        catch (e) {
            console.log(e);
        }
    }

    render() {

        const {stateData, getAllData, classes} = this.props;

        return (

            this.state.data ?
            <div className={`${wrap} ${styleForm.findForm} `} id='findForm' >
                <WithCharter GET_STATUS={stateData.GET_STATUS} GET_DATA = {getAllData} data={this.state.data}>
                    {   
                        data =>
                            <>
                           
                            <div className={[row,py__2, px__4].join(' ')} >

                                <div className={`${all__24} ${mb__1}`}>
                                    <RadioGroup onChange={data.CHANGE_WAY} value={data.way}>
                                        <Radio value={false} className={`${styleForm.fontWhite}`}>В одну сторону</Radio>
                                        <Radio value={true} className={`${styleForm.fontWhite}`}>В обе стороны</Radio>
                                    </RadioGroup>
                                </div>

                                <Popup
                                    // LABEL_TEXT= {!data.DEFAULT_FROM
                                    //     ? null : data.DEFAULT_FROM.hasOwnProperty('country')
                                    //         ?  data.DEFAULT_FROM.country.countryName : `(${data.DEFAULT_FROM.city.countryName}) ${data.DEFAULT_FROM.city.cityName}` }

                                    LABEL_TEXT = {
                                        data.DIRECTION_FROM  ? data.DIRECTION_FROM.data.UF_NAME : null

                                    }
                                    DEFAULT_DATA = {
                                        data.DIRECTION_FROM && data.DIRECTION_FROM.data.ID}


                                    PLACEHOLDER = {data.MESSAGE_CHARTER.from.placeholder}

                                    DATA={data.FROM}
                                    styles = {{
                                        root:[all__24,sm__12,lg__5].join(' '),
                                        elem:styleForm.formItem,
                                        tooltip:styleForm.tittle,
                                        icon:styleForm.icon,
                                        placeHolder: styleForm.placeHolder
                                    }}
                                    MODAL_TITLE_TEXT = {<span className={`${mr__4}`}>Укажите города вылета</span>}
                                    DATA={data.FROM}
                                    GET_DATA={data.SELECT_DIRECTION(undefined,'DIRECTION_FROM')}
                                    TEXT = {data.MESSAGE_CHARTER.from.tittle}
                                    icon = {<Place/>}
                                    DISABLE={data.FROM != null ? false : true}>
                                    <Tree2/>
                                </Popup>
                                <Popup
                                    LABEL_TEXT = {
                                        data.DIRECTION_TO ? data.DIRECTION_TO.data.UF_NAME : null
                                    }
                                    DEFAULT_DATA = {
                                        data.DIRECTION_TO && data.DIRECTION_TO.data.ID}
                                    PLACEHOLDER = {data.MESSAGE_CHARTER.to.placeholder}
                                    //DEFAULT_DATA = {data.DIRECTION_TO ? data.DIRECTION_TO : data.DEFAULT_TO}
                                    // LABEL_TEXT= {!data.DEFAULT_TO
                                    //     ? null : data.DEFAULT_TO.hasOwnProperty('country')
                                    //         ?  data.DEFAULT_TO.country.countryName : `(${data.DEFAULT_TO.city.countryName}) ${data.DEFAULT_TO.city.cityName}` }


                                    // 
                                    // DEFAULT_DATA = {!data.DEFAULT_TO
                                    //     ? null : data.DEFAULT_TO.hasOwnProperty('country')
                                    //         ?  [data.DEFAULT_TO.country.countryKey.toString()]
                                    //         : [data.DEFAULT_TO.city.cityKey.toString()]}


                                    DATA={data.TO}
                                    styles = {{
                                        root:[all__24,sm__12,lg__5].join(' '),
                                        elem:styleForm.formItem,
                                        tooltip:styleForm.tittle,
                                        icon:styleForm.icon,
                                        placeHolder: styleForm.placeHolder
                                    }}
                                    MODAL_TITLE_TEXT = {<span className={styleForm.ModalTitle}>{data.MESSAGE_CHARTER.to.popup}</span>}
                                    TEXT = {data.MESSAGE_CHARTER.to.tittle}
                                    icon = {<Place/>}
                                    GET_DATA={data.SELECT_DIRECTION(data.DIRECTION_FROM && data.DIRECTION_FROM.data.DIRECTIONS,'DIRECTION_TO')}
                                    DISABLE={data.DIRECTION_FROM ? false : true}>
                                    <Tree2/>
                                </Popup>
                                <Popup DATA={data.DATE_FROM}
                                       LABEL_TEXT={isEmpty(data.SELECTED_DATE_FROM)
                                           ? null
                                           : (data.SELECTED_DATE_FROM.from != data.SELECTED_DATE_FROM.to && data.way)
                                               ? `c ${moment(data.SELECTED_DATE_FROM.from).format('DD.MM.YYYY')} - по ${moment(data.SELECTED_DATE_FROM.to).format('DD.MM.YYYY')}`
                                                : `c ${moment(data.SELECTED_DATE_FROM.from).format('DD.MM.YYYY')}` }
                                       DEFAULT_DATA = {isEmpty(data.SELECTED_DATE_FROM) ? null : {from: new Date(_FORMAT_DATE(data.SELECTED_DATE_FROM).from), to: new Date(_FORMAT_DATE(data.SELECTED_DATE_FROM).to)}}
                                       SELECTED_DATE_FROM = {isEmpty(data.SELECTED_DATE_FROM) ? null : {from: new Date(_FORMAT_DATE(data.SELECTED_DATE_FROM).from), to: new Date(_FORMAT_DATE(data.SELECTED_DATE_FROM).to)}}
                                       PLACEHOLDER = {data.way ? data.MESSAGE_CHARTER.date.placeholder[0]: data.MESSAGE_CHARTER.date.placeholder[1]}
                                       GET_DATA={data.GET_DATA('SELECTED_DATE_FROM')}
                                       styles = {{
                                           root:[all__24,sm__12,lg__5].join(' '),
                                           elem:styleForm.formItem,
                                           tooltip:styleForm.tittle,
                                           icon:styleForm.icon,
                                           placeHolder: styleForm.placeHolder
                                       }}

                                       icon = {<Calendar/>}
                                       TEXT = {data.MESSAGE_CHARTER.date.tittle}
                                       MODAL_TITLE_TEXT = {<span className={styleForm.ModalTitle}>{data.way ? data.MESSAGE_CHARTER.date.popup[0] : data.MESSAGE_CHARTER.date.popup[1]}</span>}
                                       buttonPopupStyle = {styleForm.formItem}
                                       DISABLE={data.DIRECTION_TO  ? false : true}
                                >
                                    <DayPicker multiple={true}    DEFAULT_DATA = {isEmpty(data.SELECTED_DATE_FROM) ? null : {from: new Date(_FORMAT_DATE(data.SELECTED_DATE_FROM).from), to: new Date(_FORMAT_DATE(data.SELECTED_DATE_FROM).to)}}
                                    />
                                </Popup>
                                {
                                    data.way &&
                                    <Popup
                                        LABEL_TEXT={`${data.NIGHT[0]}-${data.NIGHT[1]}`}
                                        DATA={data.NIGHT}
                                        styles={{
                                            root: [all__24, sm__12, lg__3].join(' '),
                                            elem: styleForm.formItem,
                                            tooltip: styleForm.tittle,
                                            icon: styleForm.icon
                                        }}
                                        icon={<AccessTime/>}
                                        TEXT={data.MESSAGE_CHARTER.night.tittle}
                                        MODAL_TITLE_TEXT={
                                            <span
                                            className={styleForm.ModalTitle}>{data.MESSAGE_CHARTER.night.popup}
                                            </span>}
                                        GET_DATA={data.GET_DATA('NIGHT')}
                                        
                                    >
                                        <Slider/>
                                    </Popup>
                                }
                                <Popup
                                    LABEL_TEXT={
                                        Object.keys(data.PEOPLE).length == 0
                                            ? `${data.DEFAULT_PEOPLE.adults} взр, ${data.DEFAULT_PEOPLE.children} дет`
                                            : `${data.PEOPLE.adults} взр, ${data.PEOPLE.children} дет`
                                    }
                                   
                                    GET_DATA={data.GET_DATA('PEOPLE')}
                                    styles = {{
                                        root:[all__24,sm__12,lg__3].join(' '),
                                        elem:styleForm.formItem,
                                        tooltip:styleForm.tittle,
                                        icon:styleForm.icon
                                    }}
                                    icon = {<People/>}
                                    TEXT = {data.MESSAGE_CHARTER.people.tittle}
                                    MODAL_TITLE_TEXT = {<span className={styleForm.ModalTitle}>{data.MESSAGE_CHARTER.people.popup}</span>}
                                    DATA={data.DEFAULT_PEOPLE}>
                                    <Counter/>
                                </Popup>

                                <div className={[all__auto,flex_grow].join(' ')}>
                                    <Button onClick={data.way ? data.GET_CHARTER2 : data.GET_CHARTER2}  icon="search"
                                            disabled = { data.SELECTED_DATE_FROM  ? false : true}
                                            className={styleForm.buttonFind}
                                    >{data.MESSAGE_CHARTER.submit.tittle}
                                    </Button>
                                </div>
                            </div>
                            {
                                data.loading.status ? <Loader message={data.loading.message}/> : null
                            }
                                <div style={{color:'white'}}>{
                                    data.charter_new && JSON.stringify(data.charter_new.length)
                                }</div>
                            </>
                    }
                </WithCharter>

            </div>
            : <Loader message={'Загрузка'}/>
        );
    }
}

