import React, {Component} from 'react';
import InputMask from 'react-input-mask';
import axios from 'axios';
import {param} from 'jquery';

import {
    Form, Input, Button,
} from 'antd';


class App extends React.Component {


    handleSubmit = (e) => {
        e.preventDefault();
        const {CHARTER} = this.props;

        this.props.form.validateFields((err, values) => {
            if (!err) {

                const body = {
                    ...values,
                    ...CHARTER
                };
                this.props.onClose();
                this.props.loading();
                setTimeout(()=>{
                    axios.post(window.charterActionSend,param(body),window.AxiosPost)
                        .then(response=>{
                            this.props.loading();
                            console.log(response);
                            if(response.data["error"] == false){
                                this.props.GET_STATUS({status:'success',data:'Спасибо за бронирование'});
                            }
                            else{
                                this.props.GET_STATUS({status:'error',data:'Ошибка бронирования'});
                            }
                        }).catch(error=>{
                        this.props.loading();
                        this.props.GET_STATUS({status:'error',data:'Ошибка бронирования'});
                    });
                },1000);

            }

        });
    };



    render() {
        const { getFieldDecorator } = this.props.form;
        const {styles,GET_STATUS} = this.props;

        return (
            <>
            <Form  onSubmit={this.handleSubmit} style={{width:'100%'}} >
                <Form.Item>
                    <label htmlFor="FIO" className={`${styles.label}`}>Ф.И.О: </label>
                    {
                        getFieldDecorator('FIO', {
                        rules: [{ required: true, message: 'Пожалуйсто укажитие ваше ФИО' }],
                    })(
                        <Input  placeholder={'Укажите вашу фамилию имя и отчество'}  className={`${styles.input}`} name='FIO'  />
                    )}
                </Form.Item>
                <Form.Item>
                    <label htmlFor="phone" className={`${styles.label}`}>Номер телефона: </label>
                    {getFieldDecorator('PHONE', {
                        rules: [{ required: true, message: 'Пожалуйсто укажитие номер телефона!' }],
                    })(
                        <InputMask mask={'+375(99)999-99-99'}>
                            { inputProps => <input  type="tel" className={`${styles.input}`} {...inputProps} name="phone" placeholder={'Укажите ваш номер телефона'}  /> }
                        </InputMask>
                    )}
                </Form.Item>

                    <Button type="primary" htmlType="submit" className={`${styles.sendOpen}`} style={{width:'100%'}}>
                        Заказать
                    </Button>

            </Form>


            </>
        );
    }
}

const WrappedApp = Form.create({ name: 'coordinated' })(App);

export default  WrappedApp;