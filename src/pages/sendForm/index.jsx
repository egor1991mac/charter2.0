import React, {Component} from 'react';
import style from './style.module.css';
import InputMask from 'react-input-mask';
import Form from './form.jsx';
import {Modal, Button, Input} from 'antd';
import {mt__2, mt__3} from '../../style/indents.module.css';
import Loader from '../../component/loader';
class Index extends Component {
    state = {
        visible: false,
        loading:false
    };
    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleOk = () => {
        this.setState({
            visible: false,
        });
    };
    handleLoading = () =>{
        this.setState({loading:!this.state.loading});
    };

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    };
    render() {
        const {LABLE_TEXT, MODAL_TITLE_TEXT,CHARTER} = this.props;

        return (
            <>
                <Button className={style.sendOpen} onClick={this.showModal}>
                    {
                        LABLE_TEXT
                    }
                </Button>
                <Modal
                    title={MODAL_TITLE_TEXT}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    footer={null}
                    style={{
                        display:'flex',
                        width:'auto',
                        justifyContent:'center'
                    }}
                    bodyStyle={{
                        display:'flex',
                        justifyContent:'center',
                        alignItems:'center',
                        flexWrap:'wrap',
                        minWidth:'300px'
                    }}
                >
                    <Form styles={style} CHARTER={CHARTER} onClose = {this.handleOk} loading = {this.handleLoading}
                          GET_STATUS={this.props.stateData.GET_STATUS} />

                </Modal>
                {   this.state.loading == true
                    ? <Loader message={'Бронирование чартера'}/>
                    : null
                }
            </>
        );
    }
}

export default Index;