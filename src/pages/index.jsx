import React, {Component, Suspense, lazy} from 'react';
import WithStateData from '../hoc/withStateData';
import Loader from '../component/loader';

import {Tabs} from 'antd';
import Pagination from '../component/pagination';
import {isEmpty} from 'lodash';
import {MESSAGE_TABS} from '../config';
import 'antd/dist/antd.css';
import './style.css';
import {wrap, row} from '../style/grid.module.css';
import {Provider} from 'react-redux';
import axios from 'axios';

const TabPane = Tabs.TabPane;
const AviaForm = lazy(() => import('./AviaForm/index.jsx'));
const CharterForm = lazy(() => import('./CharterForm/index.jsx'));
const Result = lazy(() => import('./result/index.jsx'));
const SendForm = lazy(() => import('./sendForm/index.jsx'));
const TourViser = lazy(() => import('./tourviser.js'));
const BusTour = lazy(() => import('./BusForm'));
const ResultBusform = lazy(() => import('./ResultBusform'));


class CharterAvia extends Component {
    state = {
        CHARTER: null, way: false, rend: false,
        data: undefined,
        tab: window.RouterForm.defaultRouter.tab
    };
    getAllData = (way, data) => {
        this.setState({CHARTER: data, way: way});
    };

    componentWillMount() {

        window.RouterForm.routers.forEach(item => {
            const path = window.location.pathname.replace(/\//g, '');
            const tab = document.querySelector('[charterTab]');
           if(!tab){
                if (path === item.link.replace(/\//g, '')) {

                    this.setState({
                        tab: item.tab
                    });
                }
            }

        });
    }


    componentDidMount() {

        if (window.STOR != undefined) {
            this.setState({rend: !this.state.rend})
        } else {
            Object.defineProperty(window, 'STOR', {
                set: val => {
                    this.setState({rend: !this.state.rend})
                }
            })
        }
    }


    render() {
        const {CHARTER, way, rend, tab} = this.state;
        if (!document.querySelector('[charterTab]')) {
            return (
                <div className={`${wrap} tabs-container`}>
                    <div className={row}>
                        <Tabs defaultActiveKey={tab}>
                            <TabPane tab={MESSAGE_TABS.tours.title} key={MESSAGE_TABS.tours.key}>
                                <div className="tv-search-form tv-moduleid-184027" tv-resulturl="/tours/search/"></div>
                                <Suspense fallback={<Loader/>}>
                                    <TourViser/>
                                </Suspense>
                            </TabPane>
                            <TabPane tab={MESSAGE_TABS.avia.title} key={MESSAGE_TABS.avia.key}>
                                <Suspense fallback={<Loader/>}>
                                    <AviaForm/>
                                </Suspense>
                            </TabPane>
                            <TabPane tab={MESSAGE_TABS.charter.title} key={MESSAGE_TABS.charter.key}>
                                <WithStateData>
                                    {
                                        stateData => (
                                            <>
                                                <Suspense fallback={<Loader/>}>
                                                    <CharterForm stateData={stateData} getAllData={this.getAllData}
                                                                 data={this.state.data}/>
                                                </Suspense>
                                                <Suspense fallback={<Loader/>}>
                                                    {
                                                        <Pagination data={CHARTER ? CHARTER.data : null}
                                                                    count={
                                                                        CHARTER
                                                                            ? CHARTER.body.hasOwnProperty('SearchPeriodFilter')
                                                                            ? CHARTER.body.SearchPeriodFilter.count
                                                                            : CHARTER.body.searchFilter.count
                                                                            : [1, 0]}>
                                                            <Result way={way}>
                                                                <SendForm stateData={stateData} LABLE_TEXT="Заказать"
                                                                          MODAL_TITLE_TEXT="Заказ чартера"/>
                                                            </Result>
                                                        </Pagination>
                                                    }
                                                </Suspense>
                                            </>
                                        )
                                    }
                                </WithStateData>
                            </TabPane>
                            <TabPane tab={'Автобусные туры'} key={MESSAGE_TABS.busTours.key} disabled
                                     data-atr={'скоро'}>
                                {
                                    <Suspense fallback={<Loader/>}>
                                        {
                                            rend &&
                                            <Provider store={window.STOR}>
                                                <BusTour/>
                                                <ResultBusform data={'hello world'}/>
                                            </Provider>
                                        }
                                    </Suspense>
                                }
                            </TabPane>

                        </Tabs>
                    </div>

                </div>
            );
        } else {
            switch (document.querySelector('[charterTab]').getAttribute('charterTab')) {
                case 'tours':
                    return <>
                        <div className="tv-search-form tv-moduleid-184027" tv-resulturl="/tours/search/"></div>
                        <Suspense fallback={<Loader/>}>
                            <TourViser/>
                        </Suspense>
                        </>
                    break;
                case 'avia':
                    return  <Suspense fallback={<Loader/>}>
                        <AviaForm/>
                    </Suspense>
                    break;
                case 'charter':
                    return  <WithStateData>
                        {
                            stateData => (
                                <>
                                    <Suspense fallback={<Loader/>}>
                                        <CharterForm stateData={stateData} getAllData={this.getAllData}
                                                     data={this.state.data}/>
                                    </Suspense>
                                    <Suspense fallback={<Loader/>}>
                                        {
                                            <Pagination data={CHARTER ? CHARTER.data : null}
                                                        count={
                                                            CHARTER
                                                                ? CHARTER.body.hasOwnProperty('SearchPeriodFilter')
                                                                ? CHARTER.body.SearchPeriodFilter.count
                                                                : CHARTER.body.searchFilter.count
                                                                : [1, 0]}>
                                                <Result way={way}>
                                                    <SendForm stateData={stateData} LABLE_TEXT="Заказать"
                                                              MODAL_TITLE_TEXT="Заказ чартера"/>
                                                </Result>
                                            </Pagination>
                                        }
                                    </Suspense>
                                </>
                            )
                        }
                    </WithStateData>
                    break;

            }

        }

    }


}

export default CharterAvia;

