import React, {Component,lazy,Suspense} from 'react';
import {Provider} from 'react-redux';
const Appbar = lazy(()=> import('../../component/appbar'));
const Search = lazy(()=> import('../../component/searchField'));
const Logo =  lazy(()=> import('../../component/logo'));
const Nav = lazy(()=> import('../../component/nav'));
const MobileNav =  lazy(()=> import('../../component/mobileNav'));



class Header extends Component {
    state = {
        rend:false
    };
    componentDidMount() {
         if(window.STOR != undefined){
            this.setState({rend:true})
        }
        else {
            Object.defineProperty(window,'STOR',{
                set: val=>{
                    this.setState({rend:true});
                }
            })
        }
    };
    render() {
        const {rend} = this.state;
        return (
            rend ?
            <div >
                <Provider store={window.STOR}>
                    <Suspense fallback={''}>
                    <Appbar>
                        <MobileNav key = {'mobileNav'}>
                            <Search key = {'search'}/>
                        </MobileNav>
                        <Logo key = {'logo'}/>
                        <Nav key = {'nav'}>
                        </Nav>
                        <Search key = {'search'}/>
                    </Appbar>
                    </Suspense>
                </Provider>
            </div>
                : <div>Ошибка компонента</div>
        );
    }
}
export default Header;

